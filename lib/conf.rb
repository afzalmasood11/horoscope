class Conf
  @app_settings = YAML.load(File.read("#{Rails.root.to_s}/config/application.yml"))

    def self.get_api_key
      @app_settings['api_key']
    end

end