namespace :db do
  desc "Fill database with sample vouchers"
  task populate_voucher: :environment do
    Voucher.create!(code: get_voucher_code,
                 valid_days: 5,
                 is_active: false
                 )
    99.times do |n|
    	code = get_voucher_code
      valid_days = n+1
            
      Voucher.create!(
      	code: code,
      	valid_days: valid_days,
        is_active: false
       )
    end
  end

  def get_voucher_code
		begin
			voucher_code = SecureRandom.hex(4)
		end while Voucher.exists?(code: voucher_code)
		voucher_code
	end

end