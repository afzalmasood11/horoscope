require 'nokogiri'
require 'open-uri'

namespace :horoscope_daily do

	def log content
		puts "#{Time.now.to_s}\t#{content}"
	end
	
	desc "Import Daily horoscope data"
	task :import_xml => :environment do
		log "Import start"
		horoscope_daily_URL="http://api.viversum.de/xml/horoscopeDayFormal?apikey=4ce07ba13c076199c1efd25c23e3fe33&calcDate="
		horscope_daily_date = Date.today
		7.times do |i|
			begin
				doc = Nokogiri::XML(open(horoscope_daily_URL+(horscope_daily_date + i).to_s).read)
				horoscopes = doc.xpath('root/horoscope/zodiacSign')   
				horoscopes.each do |horoscope|  
					zodiac_name = horoscope.xpath('name').text
					zodiac = Zodiac.where(name: zodiac_name).first
					return "Zodiac not found for #{zodiac_name}" if zodiac.blank?
					horoscope_hash = {
						date: doc.xpath('root/validUntil').text, published: false,
						trend: horoscope.xpath("section[1]/content").text, money: horoscope.xpath("section[2]/content").text,
						love: horoscope.xpath("section[3]/content").text, health: horoscope.xpath("section[4]/content").text,
						tip: horoscope.xpath("section[5]/content").text
					}
					@horoscope_daily = zodiac.horoscope_dailies.build(horoscope_hash)
					@horoscope_daily.save!
				end  

				if @horoscope_daily.save
					log "horroscope_Daily.import_xml \t#{(horscope_daily_date + i).to_s}\t success."      
				else
					message = "horroscope_Daily.import_xml\t#{(horscope_daily_date + i).to_s}\tUngültige Daten."
					log message
					Notifier.exception(message).deliver
       		break
				end
      rescue Exception => e
       Notifier.exception(e).deliver
       break
			end 
		end 
	end 

end

namespace :deploy do
	# desc "Update the crontab file"
	# task :update_crontab do
	# 	puts "--Rails.root----#{Rails.root}"
 #    # exec "cd #{Rails.root} && whenever --update-crontab trunk"
 #    # %x(cd #{Rails.root} && whenever --update-crontab trunk)
 #    # system "cd #{Rails.root} && whenever --update-crontab trunk"

 #  end

  desc "Update the crontab file"
  task :update_crontab do
  	# run "cd #{release_path} && whenever --update-crontab #{application}"
  	`run "cd #{Rails.root} && whenever --update-crontab trunk"`
  end
end