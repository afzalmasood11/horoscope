class Notifier < ActionMailer::Base
  default from: $APP_SETTING[:email]

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.notifier.exception.subject
  #
  def exception(message='Exception Occured')
    @greeting = "Hi"
    @message = message
    options = {to: $APP_SETTING[:email], subject: $APP_SETTING[:email_subject]}
    options.merge!({cc: $APP_SETTING[:cc]}) if $APP_SETTING[:cc]    
    mail options
  end
end
