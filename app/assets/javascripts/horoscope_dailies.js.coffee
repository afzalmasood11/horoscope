# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
#$('.datepicker').datepicker()

d = new Date()
currDate = d.getDate()
currMonth = d.getMonth()
currYear = d.getFullYear()
dateStr = currYear + "-" + currMonth + "-" + currDate
$ ->
  $("#datepicker").datepicker(
    format: "yyyy-mm-dd"
    update: true
    autoclose: true
    autoSize: true
    defaultDate: dateStr
    changeDate: (dateText, inst) ->
      $("table-form").submit()
  ).on "changeDate", (ev) ->
    $(this).datepicker "hide"
    # $("#table-form").submit()
    date = $("#datepicker").val()
    
    $.ajax
      type: "GET"
      dataType: "html"
      url: "/horoscope_dailies"
      data:
        date: date

      success: (data) ->
        $("#index_content").html data

      error: (object, error) ->
        console.log error


# get_ids = (val) ->
#   ids = ""
#   $(".published:checked").each ->

#     #checking is it already published/unpublished or not
#     ids = ids + $(this).val() + ","  if $(this).parent().next().text() is val

#   ids
# publish_unpublish = (val, text_val) ->
#   if $(".published:checked").length > 0
#     ids_str = get_ids(val)
#     if ids_str.length > 0
#       bVal = (if val is "true" then not val else !!val)
#       $.ajax
#         type: "POST"
#         dataType: "html"
#         url: "/horoscope_dailies/publish_unpub_horoscope"
#         data:
#           publish: ids_str
#           b_val: bVal
#           date: $("#datepicker").val()

#         success: (data) ->
#           $("#index_content").html data

#         error: (object, error) ->
#           console.log error

#     else
#       alert "Already " + text_val + "ed."
#   else
#     alert "Please Select anyone to " + text_val + "."
# $ ->
#   $("#pub_btn").click ->
#     publish_unpublish "false", "publish"

#   $("#unpub_btn").click ->
#     publish_unpublish "true", "unpublish"

$ ->
  $("#select-deselect-all").click ->
    if $(".published:checked").length < $(".published").length
      value = true
    else value = false  if $(".published:checked").length is $(".published").length
    $(".published").each ->
      $(this).prop "checked", value
      $(this).parent().toggleClass "checked", value

$ ->
  $("[data-behaviour~=datepicker]").datepicker().on "changeDate", (e) ->

    date = $("#horoscope_daily_date").val()
    z_id = $("#zodiac_id").val()
    h_id = $("#horo_m_id").val()

    $.ajax
      type: "POST"
      dataType: "html"
      url: "/horoscope_dailies/zodiac_names"
      data:
        date: date
        h_id: h_id
        z_id: z_id
        
      success: (data) ->
        $("#horoscope_daily_zodiac_id").html data

      error: (object, error) ->
        console.log error
