# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

d = new Date()
currDate = d.getDate()
currMonth = d.getMonth()
currYear = d.getFullYear()
dateStr = currYear + "-" + currMonth + "-" + currDate
$ ->
  $("#datepicker-weekly").datepicker(
    format: "yyyy-mm-dd"
    update: true
    autoclose: true
    weekStart: 1
    calendarWeeks: true  
    autoSize: true
    defaultDate: dateStr
    changeDate: (dateText, inst) ->
      $("table-form").submit()
  ).on "changeDate", (ev) ->
    $(this).datepicker "hide"
    # $("#table-form").submit()
    date = $("#datepicker-weekly").val()
    $("#new-link").attr "href", $("#new-link").attr("href") + "?date=" + $("#datepicker-weekly").val()
    $.ajax
      type: "GET"
      dataType: "html"
      url: "/horoscope_weeklies"
      data:
        date: date

      success: (data) ->
        $("#index_content").html data

      error: (object, error) ->
        console.log error

$ ->
  $("[data-behaviour~=weekly-datepicker]").datepicker().on "changeDate", (e) ->
    date = $("#horoscope_weekly_date").val()
    z_id = $("#zodiac_id").val()
    h_id = $("#horo_m_id").val()

    $.ajax
      type: "POST"
      dataType: "html"
      url: "/horoscope_weeklies/zodiac_names"
      data:
        date: date
        h_id: h_id
        z_id: z_id

      success: (data) ->
        $("#horoscope_weekly_zodiac_id").html data

      error: (object, error) ->
        console.log error
