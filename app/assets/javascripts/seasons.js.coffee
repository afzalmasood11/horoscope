# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
startDate = new Date("01/01/2012")
FromEndDate = new Date()
ToEndDate = new Date()
ToEndDate.setDate ToEndDate.getDate() + 3265000

$ ->
	$("#season_date_from").datepicker(
	  format: "yyyy-mm-dd"
	  weekStart: 1
	  startDate: "01/01/2012"
	  autoclose: true
	).on "changeDate", (selected) ->
	  startDate = new Date(selected.date.valueOf())
	  startDate.setDate startDate.getDate(new Date(selected.date.valueOf()))
	  $("#season_date_to").datepicker "setStartDate", startDate

	$("#season_date_to").datepicker(
	  format: "yyyy-mm-dd"
	  weekStart: 1
	  startDate: startDate
	  endDate: ToEndDate
	  autoclose: true
	).on "changeDate", (selected) ->
	  FromEndDate = new Date(selected.date.valueOf())
	  FromEndDate.setDate FromEndDate.getDate(new Date(selected.date.valueOf()))
	  $("#season_date_from").datepicker "setEndDate", FromEndDate