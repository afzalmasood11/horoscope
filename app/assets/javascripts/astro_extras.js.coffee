# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
$ ->
  $(".preview_image").change ->
    readURL this

  $(".remove_image").click ->  
    removeURL this, $(".preview_image")

readURL = (input) ->
  if input.files and input.files[0]
    reader = new FileReader()
    reader.readAsDataURL input.files[0]
    reader.onload = (e) ->
      $("#img_prev").attr("src", e.target.result).width(200).height(150).css display: "block"
      $('.remove_image').css display: "block"
      $('.remove_uploaded').css display: "none"

removeURL = (remove_button, input) ->
    $(input).val('')
    $("#img_prev").css display: "none"
    $(remove_button).css display: "none"
    $('.remove_uploaded').css display: "block"

$ ->
  $("#astro_extra_season_id").change ->
    season_id = $("#astro_extra_season_id").val()
    
    $.ajax
      type: "POST"
      dataType: "html"
      url: "/astro_extras/zodiac_names"
      data:
        season_id: season_id

      success: (data) ->
        $("#astro_extra_zodiac_id").html data

      error: (object, error) ->
        console.log error