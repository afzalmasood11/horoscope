module HoroscopeMonthliesHelper
	def moon_phase_data(horoscope)
		moon_phase = horoscope.moon_phases.where(phase_name: "full_moon").first
		moon_phase.data if moon_phase.present?
	end
end
