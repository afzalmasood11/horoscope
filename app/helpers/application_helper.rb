module ApplicationHelper
#def display_base_errors resource
#    return '' if (resource.errors.empty?) or (resource.errors[:base].empty?)
#    messages = resource.errors[:base].map { |msg| content_tag(:p, msg) }.join
#    html = <<-HTML
#    <div class="alert alert-error alert-block">
#      <button type="button" class="close" data-dismiss="alert">&#215;</button>
#{messages}
#  </div>
    #HTML
#    html.html_safe
#  end
  
	def flash_class(level)
	  case level
	    when :notice then "alert alert-info"
	    when :success then "alert alert-success"
	    when :error then "alert alert-error"
	    when :alert then "alert alert-alert"
	  end
	end

	def image_preview(f)
	    url, style =  f.object.image.present? ?  [f.object.image.url(:thumb), ''] : ['#','display:none;']
	    content_tag(:div, nil) do
		    concat image_tag url , {id: 'img_prev', style: style}
		    concat '</br>'.html_safe
		    concat  f.button 'Remove', {type: 'button', class: 'remove_image', style:'display:none;'}
	    end
	end
	
end
