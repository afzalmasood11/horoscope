class Device < ActiveRecord::Base

	before_create :generate_token

	attr_accessible :device_id, :is_active, :token, :valid_days, :valid_upto, :horoscope_validity, :rest_of_horoscope_validity

	validates :token, :uniqueness => true
	validates :device_id, :uniqueness => true

  MODULES_NAME = ['horoscope', 'rest_of_horoscope']
  
  has_many :devices_iap
  has_many :in_app_purchases, through: :devices_iap

	private

	def generate_token
		begin
			self.token = SecureRandom.hex
		end while self.class.exists?(token: token)
	end

end
