class UserLexicon < ActiveRecord::Base

  attr_accessible :date, :email, :is_active, :issue, :reply, :user

	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :issue, presence: {message: "can't be blank."}
  validates :user, presence: {message: "can't be blank."}
  validates :email, presence: {message: "can't be blank."}, format: { with: VALID_EMAIL_REGEX }
	
  validate :published_validator

	self.per_page = 10 

  def published_validator
    if self.reply.blank? and self.is_active
      errors.add(:reply, "must before publish")
    end
  end

	def as_json(options={})
    {
    	question: self.issue,
      answer: self.reply
    }
  end

end
