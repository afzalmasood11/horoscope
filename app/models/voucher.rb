class Voucher < ActiveRecord::Base

  attr_accessible :name, :code, :is_active, :valid_days

  validates :code, uniqueness: true, presence: {message: "can't be blank."}, length: { minimum: 8 , maximum: 12, message: "should be in between 8 - 12." }
  
  validates :valid_days, presence: {message: "can't be blank."}, numericality: { greater_than_or_equal_to: 1, message: "must be greate or equal to 1." }
  validates :is_active, presence: true, :if => 'is_active.nil?'

  self.per_page = 10

end
