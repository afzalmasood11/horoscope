class HoroscopeDaily < ActiveRecord::Base
	
	include Base::HoroscopeZodiac

	attr_accessible  :zodiac_id,:date, :published, :trend, :money, :love, :health, :tip
  
  belongs_to  :zodiac

  scope :all_zodiac_by_date, lambda {|date| joins("RIGHT OUTER JOIN zodiacs ON zodiacs.id = horoscope_dailies.zodiac_id and horoscope_dailies.date = '#{date.to_s}'").select(' horoscope_dailies.*, zodiacs.name as zodiac_name') }
  scope :all_zodiac_by_published, lambda {|date| joins("RIGHT OUTER JOIN zodiacs ON zodiacs.id = horoscope_dailies.zodiac_id and horoscope_dailies.date = '#{date.to_s}' and horoscope_dailies.published = true").select(' horoscope_dailies.*, zodiacs.name as zodiac_name') }
  
  validates :date, :presence => {:message => "Can't be blank."}
  validates :published, :presence => {:message => "Can't be blank."}, :if => 'published.nil?'
  validate :date_validator
  validates :zodiac_id, uniqueness: { scope: :date } # need to write the test case for this.

  def date_validator
  	begin
  		date.to_date
  	rescue  Exception => e
  		errors.add(:date, "is invalid")
  		return false
  	end
  end

  # def as_json(options = {})       
  #   { zodiac: self.zodiac,
  #     name: self.zodiac_name,
  #     date: self.date,
  #     horoscope_daily: super(:only => [:trend, :tip, :money, :love, :health]),
  #     horoscope_weekly: {},
  #     # horoscope_monthly: options[:monthlies]
  #     horoscope_monthly: HoroscopeMonthly.zodiac_by_date_and_name(self.date, self.zodiac_name)
  #   }
  # end
  def as_json(options = {})       
    super(:only => [:trend, :tip, :money, :love, :health])
  end

  

  
end
