class AstroExtra < ActiveRecord::Base
	# include Base::HoroscopeZodiac

	belongs_to :zodiac
	belongs_to :season

  attr_accessible :title, :zodiac_id, :season_id, :horoscope, :image, :is_active, :remove_image

  mount_uploader :image, ImageUploader

  validates :title, presence: {message: "can't be blank."}
  validates :zodiac_id, presence: {message: "can't be blank."}
  validates :season_id, presence: {message: "can't be blank."}

  class << self
	  def get_seasons
      Season.select("id, name").collect{|obj| [obj.name, obj.id] }
	  end

    def get_zodiac(options, object = nil )
      options[:date] ||= (object.present? ? object.date : Date.today.to_s) if options.has_key?(:date)
      ids = where(options).collect(&:zodiac_id)
      ids.delete(object.zodiac_id) if object.present? and object.season_id == options[:season_id]
      ids.blank? ? Zodiac.all : Zodiac.where("id NOT IN (?)", ids)
    end

  end

  def as_json(options={})
    { zodiac: self.zodiac.zodiac,
      name: self.zodiac.name,
      season_name: self.season.name,
      text: self.horoscope,
      image_url: (self.image.url) ? $APP_SETTING[:host].to_s + self.image.url.to_s : nil
    }
  end

end
