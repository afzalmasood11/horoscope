class InAppPurchase < ActiveRecord::Base
  attr_accessible :module_name, :production_id, :sandbox_id
  
  has_many :devices_iap
  has_many :devices, through: :devices_iap
  
  validates :module_name, presence: {message: "can't be blank."}

end
