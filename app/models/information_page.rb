class InformationPage < ActiveRecord::Base
  attr_accessible :body, :name, :title,:englishname, :url_link
  
  def as_json(options = {})				
    { 
       self.englishname => {:name => self.name , :url => self.url_link , :updated_on => self.updated_at }				
    }
  end 
end
