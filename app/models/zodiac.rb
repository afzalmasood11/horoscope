class Zodiac < ActiveRecord::Base  
	
	has_many :horoscope_dailies
	has_many :horoscope_weeklies
  has_many :horoscope_monthlies
  
  has_many :astro_extras

  attr_accessible :name, :zodiac
    
  def self.zodiac_id search_string 
    z_id=""
    results =Zodiac.select(:id).where("name ilike ?","#{search_string}%")
    results.each do |row|
      z_id = row[:id]
    end
    return z_id
  end
  
end
