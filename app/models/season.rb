class Season < ActiveRecord::Base

	has_many :astro_extras

  attr_accessible :name, :date_from, :date_to, :year

  before_validation :add_year

  # SEASON_NAME = ["Spring", "Winter", "Autumn", "Summer"]
  
  validates :date_from, :date_to, overlap: {:message_title => "Duration", :message_content => "can't be overlap with other season."}
  validates :date_from, presence: {message: "can't be blank"}
  validates :date_to, presence: {message: "can't be blank"}
  validates :name, presence: {message: "can't be blank"}, 
  	uniqueness: { scope: :year, message: " of season must be unique for a year."}
  
  validate :season_period

  def season_period
  	if date_from.present? and date_to.present? and (date_from.to_date > date_to.to_date)
  		errors.add(:date_from, "should be less than date_to")
  		return false
  	end
  end

  def add_year
  	self.year = self.date_from.to_date.year.to_s if self.date_from.present?
  end

end
