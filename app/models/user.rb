class User < ActiveRecord::Base
  rolify
  validates :username, presence: true
  validates :username, uniqueness: true, if: -> { self.username.present? }
    # Include default devise modules. Others available are:
    # :token_authenticatable, :confirmable,
    # :lockable, :timeoutable and :omniauthable
    #devise :database_authenticatable, :registerable,
    #       :recoverable, :rememberable, :trackable, :validatable
    # attr_accessible :title, :body
  
    #devise :database_authenticatable, :recoverable, :rememberable, :trackable, :validatable
    devise :database_authenticatable,  :rememberable, :trackable, :validatable, :authentication_keys => [:login]
    #attr_accessible :title, :body
    attr_accessible :role_ids
    attr_accessible :role_ids,  :as => :admin
    #attr_accessible :registerable,  :as => :admin
    #attr_accessible :name, :email, :password, :password_confirmation, :remember_me
    attr_accessible  :username,:name, :email, :password, :password_confirmation, :remember_me
    attr_accessible :login
    attr_accessor :login

  
    def self.find_first_by_auth_conditions(warden_conditions)
      conditions = warden_conditions.dup
      if login = conditions.delete(:login)
        where(conditions).where(["lower(username) = :value OR lower(email) = :value", { :value => login.downcase }]).first
      else
        where(conditions).first
      end
    end
    def email_required?
      false
    end

    def email_changed?
      false
    end
  end
