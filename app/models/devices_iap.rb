class DevicesIap < ActiveRecord::Base
  belongs_to :device
  belongs_to :in_app_purchase
  attr_accessible :expire_at, :receipt_data, :req_time, :status, :device_id, :in_app_purchase_id

  validates :receipt_data, presence: {message: "can't be blank."}

end
