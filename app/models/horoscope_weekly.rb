class HoroscopeWeekly < ActiveRecord::Base

  attr_accessible :date, :zodiac_id, :published, :trends, :dates, :tip
  belongs_to  :zodiac
  
  scope :all_zodiac_by_date, lambda {|date| joins("RIGHT OUTER JOIN zodiacs ON zodiacs.id = horoscope_weeklies.zodiac_id and horoscope_weeklies.date between date_trunc('week', '#{date.to_s}'::date) and  
 (date_trunc('week', '#{date.to_s}'::date)+ '6 days'::interval)::date ").select(' horoscope_weeklies.*, zodiacs.name as zodiac_name') }

  scope :all_zodiac_by_published, lambda {|date| joins("RIGHT OUTER JOIN zodiacs ON zodiacs.id = horoscope_weeklies.zodiac_id and horoscope_weeklies.published = true and horoscope_weeklies.date between date_trunc('week', '#{date.to_s}'::date) and  
 (date_trunc('week', '#{date.to_s}'::date)+ '6 days'::interval)::date ").select(' horoscope_weeklies.*, zodiacs.name as zodiac_name') }
  
  validates :date, :presence => {:message => "Can't be blank."}
  validates :published, :presence => {:message => "Can't be blank."}, :if => 'published.nil?'
  validate :date_validator

  def date_validator
  	begin
  		date.to_date
  	rescue  Exception => e
  		errors.add(:date, "is invalid")
  		return false
  	end
  end

  def as_json(options = {})       
    super(only: [:trends, :dates, :tip])
  end

  def self.zodiac_list(given_date, object=nil)
    given_date ||= (object.present? ? object.date : Date.today)   
    given_date = given_date.to_date
    # ids = HoroscopeWeekly.by_week(given_date.strftime("%W").to_i, year: given_date.year, field: date).collect(&:zodiac_id)
    ids = HoroscopeWeekly.by_given_date(given_date).collect(&:zodiac_id)
    ids.delete(object.zodiac_id) if object.present? and object.date.to_date.strftime("%W").to_i == given_date.strftime("%W").to_i
    ids.blank? ? Zodiac.all : Zodiac.where("id NOT IN (?)", ids)
  end

  def self.by_given_date(given_date)
    HoroscopeWeekly.where("date between date_trunc('week', '#{given_date.to_s}'::date) and (date_trunc('week', '#{given_date.to_s}'::date)+ '6 days'::interval)::date ")
  end

end
