class Api::Horoscope

  class << self
    
    def get_default_horo_daily
      {
        trend: nil,
        tip: nil,
        money: nil,
        love: nil,
        health: nil
      }
    end

    def get_default_horo_monthly
      {
        full_moon: nil,
        quarter_moon_1: nil,
        new_moon: nil,
        quarter_moon_2: nil,
        health: nil
      }
    end

    def get_default_horo_weekly
      {
        trends: nil,
        dates: nil,
        tip: nil
      } 
    end 

    def response_builder
      date = Date.today
        horoscopes_dailies =  HoroscopeDaily.all_zodiac_by_published(date)
        horoscopes_monthlies =  HoroscopeMonthly.includes(:moon_phases).all_zodiac_by_published(date)
        horoscopes_weeklies =  HoroscopeWeekly.all_zodiac_by_published(date)
        

        response = []
        Zodiac.all.each_with_index do |zodiac, index|
          response << {
            zodiac: zodiac.zodiac,
            name: zodiac.name,
            date: date,
            horoscope_daily: (horoscopes_dailies[index].present? ? horoscopes_dailies[index].as_json : get_default_horo_daily),
            horoscope_monthly: (horoscopes_monthlies[index].moon_phases.present? ? horoscopes_monthlies[index].as_json : get_default_horo_monthly),
            horoscope_weekly: (horoscopes_weeklies[index].present? ? horoscopes_weeklies[index].as_json : get_default_horo_weekly)
          }
        end
        response
    end

  end # self 

end