class DeviceVoucher < ActiveRecord::Base

  attr_accessible :device_id, :voucher_id

  validates :device_id, presence: true
  validates :voucher_id, presence: true
  validates :voucher_id, uniqueness: { scope: :device_id }  

end
