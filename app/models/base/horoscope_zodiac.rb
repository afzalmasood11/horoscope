module Base::HoroscopeZodiac
	
	module ClassMethods
		# def get_zodiac(date, object = nil)
		# 	date ||= object.present? ?  object.date : Date.today
		# 	ids = where(date: date).collect(&:zodiac_id)
		# 	ids.delete(object.zodiac_id) if object.present?
	 #  	ids.blank? ? Zodiac.all : Zodiac.where("id NOT IN (?)", ids)
		# end
		def get_zodiac(options, object = nil )
			options[:date] ||= (object.present? ? object.date : Date.today.to_s) if options.has_key?(:date)
			ids = where(options).collect(&:zodiac_id)
			ids.delete(object.zodiac_id) if object.present? and object.date.to_s == options[:date].to_s
	  	ids.blank? ? Zodiac.all : Zodiac.where("id NOT IN (?)", ids)
		end
	end
	
	module InstanceMethods
  end
	

	def self.included(receiver)
		receiver.extend         ClassMethods
		receiver.send :include, InstanceMethods
	end
end
