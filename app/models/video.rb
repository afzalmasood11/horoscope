class UrlValidator < ActiveModel::EachValidator
 
  def validate_each(record, attribute, value)
    valid = begin
      URI.parse(value).kind_of?(URI::HTTP)
    rescue URI::InvalidURIError
      false
    end
    unless valid
      record.errors[attribute] << (options[:message] || "is an invalid URL")
    end
  end
 
end


class Video < ActiveRecord::Base
  attr_accessible :description, :is_active, :title, :url

  validates :url, presence: true, url: true
  validates :title, presence: {message: "can't be blank."}

  self.per_page = 10

  def as_json(options={})
    { video_url: self.url,
      title: self.title,
      description: self.description 
    }
  end

end
