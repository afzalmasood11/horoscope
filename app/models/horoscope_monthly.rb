class HoroscopeMonthly < ActiveRecord::Base
  
  by_star_field :date

  has_many :moon_phases, order: 'priority'

  accepts_nested_attributes_for :moon_phases, allow_destroy: true

  attr_accessible :date, :published, :zodiac_id, :moon_phases_attributes

  belongs_to  :zodiac

  scope :all_zodiac_by_date, lambda {|date| joins("RIGHT OUTER JOIN zodiacs ON zodiacs.id = horoscope_monthlies.zodiac_id and date_trunc('month', horoscope_monthlies.date) = date_trunc('month','#{date.to_s}'::date)").select(' horoscope_monthlies.*, zodiacs.name as zodiac_name') }
  scope :all_zodiac_by_published, lambda {|date| joins("RIGHT OUTER JOIN zodiacs ON zodiacs.id = horoscope_monthlies.zodiac_id and date_trunc('month', horoscope_monthlies.date) = date_trunc('month','#{date.to_s}'::date) and horoscope_monthlies.published = true").select(' horoscope_monthlies.*, zodiacs.name as zodiac_name') }
  scope :zodiac_by_date_and_name, lambda {|date, zodiac_name| joins("RIGHT OUTER JOIN zodiacs ON zodiacs.id = horoscope_monthlies.zodiac_id and  date_trunc('month', horoscope_monthlies.date)  =  date_trunc('month','#{date.to_s}'::date)").where(["zodiacs.name = '#{zodiac_name.downcase.camelcase}'"]).select(' horoscope_monthlies.*, zodiacs.name as zodiac_name') }

  validates :date, :presence => {:message => "Can't be blank."}
  validates :published, :presence => {:message => "Can't be blank."}, :if => 'published.nil?'
  validate :date_validator

  def date_validator
  	begin
  		date.to_date
  	rescue  Exception => e
  		errors.add(:date, "is invalid")
  		return false
  	end
  end

  def as_json(options = {})       
    hash = {}
    self.moon_phases.each do |moon|
       hash[moon.phase_name] = moon.data
    end
    hash
  end

  def self.zodiac_list(date, object=nil)
    date ||= (object.present? ? object.date : Date.today)
    date = date.to_date
    ids = HoroscopeMonthly.by_month(date.month, year: date.year).collect(&:zodiac_id)
    ids.delete(object.zodiac_id) if object.present? and object.date.to_date.month == date.month
    ids.blank? ? Zodiac.all : Zodiac.where("id NOT IN (?)", ids)
  end

end
