class MoonPhase < ActiveRecord::Base

  belongs_to :horoscope_monthly_id
  
  attr_accessible :data, :phase_name, :priority

  NAME = ["full_moon", "quarter_moon_1", "new_moon", "quarter_moon_2", "health"]

  validates :phase_name, presence: {message: "can't be blank."}
  validates :priority, presence: {message: "can't be blank."}

  def as_json(options = {})
    {  "#{self.phase_name}" => self.data  }
  end

end
