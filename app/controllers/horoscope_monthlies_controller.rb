class HoroscopeMonthliesController < ApplicationController
  
  before_filter :date_validator
  before_filter :get_zodiac, only: :new

  def index
    params[:date] = Date.today if params[:date].blank?
    @horoscope_monthlies = HoroscopeMonthly.all_zodiac_by_date(params[:date])
    render partial: 'indexform' if request.xhr?
  end

  def show
    @horoscope_monthly = HoroscopeMonthly.find(params[:id])
  end

  def new
    @horoscope_monthly = HoroscopeMonthly.new
    5.times do |i|
      @horoscope_monthly.moon_phases.build(phase_name: MoonPhase::NAME[i], priority: (i + 1) )
    end
  end

  def edit
    @horoscope_monthly = HoroscopeMonthly.includes(:moon_phases).find(params[:id])
    get_zodiac
  end

  def create
    @horoscope_monthly = HoroscopeMonthly.new(params[:horoscope_monthly])

    respond_to do |format|
      if @horoscope_monthly.save
        format.html { redirect_to horoscope_monthlies_path, notice: 'Horoscope monthly was successfully created.' }
        format.json { render json: @horoscope_monthly, status: :created, location: @horoscope_monthly }
      else
        format.html { render action: "new" }
        format.json { render json: @horoscope_monthly.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @horoscope_monthly = HoroscopeMonthly.find(params[:id])

    respond_to do |format|
      if @horoscope_monthly.update_attributes(params[:horoscope_monthly])
        format.html { redirect_to horoscope_monthlies_path, notice: 'Horoscope monthly was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @horoscope_monthly.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @horoscope_monthly = HoroscopeMonthly.find(params[:id])
    @horoscope_monthly.destroy

    respond_to do |format|
      format.html { redirect_to horoscope_monthlies_url }
      format.json { head :no_content }
    end
  end

  def zodiac_names    
    date = params[:date].to_date
    horoscope_monthlies = HoroscopeMonthly.by_month(date.month, year: date.year)
    ids = horoscope_monthlies.collect(&:zodiac_id)
    ids.delete(params[:z_id].to_i) if ids.present? and params[:z_id].present? and params[:h_id].present? and horoscope_monthlies.collect(&:id).include?(params[:h_id].to_i)    
    @zodiacs = ids.blank? ? Zodiac.all : Zodiac.where("id NOT IN (?)", ids)

    render :partial => 'shared/zodiacs'
  end

  # To publish the array of horoscope 
  def publish_unpub_horoscope
    id = params[:publish].split(',')
    @published_horoscope_monthlies = HoroscopeMonthly.where("id IN (?)", id)
    @published_horoscope_monthlies.update_all(published: params[:b_val] )   
    
    params[:date] ||= @published_horoscope_monthlies.first.date
    @horoscope_monthlies = HoroscopeMonthly.all_zodiac_by_date(params[:date])
    render :partial => 'indexform'    
  end

  def get_zodiac
    @zodiacs = HoroscopeMonthly.zodiac_list(params[:date], @horoscope_monthly)
  end

end
