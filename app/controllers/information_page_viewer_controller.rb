class InformationPageViewerController < ApplicationController
  skip_before_filter :authenticator, :only => :show
  def show
    @information_page = InformationPage.find_by_name(params[:name])
    respond_to do |format| 
      format.html    { render :layout => false}
    end
  end
end
