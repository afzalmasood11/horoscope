class UsersController < ApplicationController
   before_filter :authenticate_user!

  def index
     authorize! :index, @user, :message => 'Not authorized as an administrator.'
    @users = User.all
  end

  def show
   authorize! :update, @user, :message => 'Not authorized as an administrator.'
    @user = User.find(params[:id])
    unless @user == current_user
       @user = User.find(params[:id])
     else
      redirect_to users_path, :notice => "Not authorized "
    end 
  end

  def new
    authorize! :update, @user, :message => 'Not authorized as an administrator.'
    @user = User.new
  end

  def edit
    authorize! :update, @user, :message => 'Not authorized as an administrator.'
    @user = User.find(params[:id])
     unless @user == current_user
       @user = User.find(params[:id])
     else
      redirect_to users_path, :notice => "Can't edit yourself."
    end 
  end

  def create
    authorize! :update, @user, :message => 'Not authorized as an administrator.'
    @user = User.new(params[:user])
    if @user.save
      #redirect_to @user, :flash => { :success => 'User was successfully created.' }
      redirect_to users_path, :flash => { :success => 'User was successfully created.' }
    else
      render :action => 'new'
    end
  end

  def update
    authorize! :update, @user, :message => 'Not authorized as an administrator.'
    @user = User.find(params[:id])
    
    if @user.update_attributes(params[:user])           
      redirect_to users_path, :notice => "User updated."      
    else
      redirect_to users_path, :alert => "Unable to update user."
    end
     
  #  authorize! :update, @user, :message => 'Not authorized as an administrator.'
  #  @user = User.find(params[:id])

  #  if @user.update_attributes(params[:user])
   #   sign_in(@user, :bypass => true) if @user == current_user
  #    redirect_to @user, :flash => { :success => 'User was successfully updated.' }
  #  else
   #   render :action => 'edit'
  #  end
  end

  def destroy
     authorize! :destroy, @user, :message => 'Not authorized as an administrator.'
    user = User.find(params[:id])
    unless user == current_user
      user.destroy
      redirect_to users_path, :notice => "User deleted."
    else
      redirect_to users_path, :notice => "Can't delete yourself."
    end
  end
  #  authorize! :destroy, @user, :message => 'Not authorized as an administrator.'
  #  @user = User.find(params[:id])
  #  @user.destroy
   # redirect_to users_path, :flash => { :success => 'User was successfully deleted.' }
   #end
end