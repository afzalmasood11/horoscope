class VideosController < ApplicationController
  
  def index
    get_videos
  end

  def show
    @video = Video.find(params[:id])
  end

  def new
    @video = Video.new
  end

  def edit
    @video = Video.find(params[:id])
  end

  def create
    @video = Video.new(params[:video])

    respond_to do |format|
      if @video.save
        format.html { redirect_to videos_path, notice: 'Video was successfully created.' }
        format.json { render json: @video, status: :created, location: @video }
      else
        format.html { render action: "new" }
        format.json { render json: @video.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @video = Video.find(params[:id])

    respond_to do |format|
      if @video.update_attributes(params[:video])
        format.html { redirect_to videos_path, notice: 'Video was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @video.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @video = Video.find(params[:id])
    @video.destroy

    respond_to do |format|
      format.html { redirect_to videos_url }
      format.json { head :no_content }
    end
  end

  # To publish video
  def pub_unpub_video
    
    uri = URI.parse(request.headers["Referer"])

    parameters = {:page => 1}
    parameters[:page] =  CGI.parse(uri.query)['page'].first if uri.query.present?
    params.merge!(parameters)


    video = Video.find(params[:id])
    video.update_attributes(is_active: params[:is_active] )   
    get_videos

    render :partial => 'videos'

  end

  private
  
  def get_videos
    @videos = Video.order('id').paginate(page: params[:page])
  end

end
