class VouchersController < ApplicationController	
  
  def index
    get_vouchers
  end

  def show
    @voucher = Voucher.find(params[:id])
  end

  def new
    @voucher = Voucher.new
  end

  def edit
    @voucher = Voucher.find(params[:id])
  end

  def create
    @voucher = Voucher.new(params[:voucher])

    respond_to do |format|
      if @voucher.save
        format.html { redirect_to @voucher, notice: 'Voucher was successfully created.' }
        format.json { render json: @voucher, status: :created, location: @voucher }
      else
        format.html { render action: "new" }
        format.json { render json: @voucher.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @voucher = Voucher.find(params[:id])

    respond_to do |format|
      if @voucher.update_attributes(params[:voucher])
        format.html { redirect_to @voucher, notice: 'Voucher was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @voucher.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @voucher = Voucher.find(params[:id])
    @voucher.destroy
    flash[:success] = "Voucher destroyed."
    
    respond_to do |format|
      format.html { redirect_to vouchers_url }
      format.json { head :no_content }
    end
  end

  # To publish voucher 
  def pub_unpub_voucher 
  	
  	uri = URI.parse(request.headers["Referer"])

  	parameters = {:page => 1}
  	parameters[:page] =  CGI.parse(uri.query)['page'].first if uri.query.present?
  	params.merge!(parameters)


  	voucher = Voucher.find(params[:id])
  	voucher.update_attributes(is_active: params[:is_active] ) 	
  	get_vouchers

  	render :partial => 'vouchers'

  end

  private
  def get_vouchers
  	@vouchers = Voucher.order('id').paginate(page: params[:page])  	
  end

end
