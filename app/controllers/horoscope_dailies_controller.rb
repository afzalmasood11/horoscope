class HoroscopeDailiesController < ApplicationController
	before_filter :date_validator
	before_filter :get_zodiac, only: :new

	def index  
		params[:date] = Date.today if params[:date].blank?
		@horoscope_dailies = HoroscopeDaily.all_zodiac_by_date(params[:date])
		
		render partial: 'indexform' if request.xhr?
  end

  
  def show
  	@horoscope_daily = HoroscopeDaily.find(params[:id])

  	respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @horoscope_daily }
    end
  end

  def new
  	@horoscope_daily = HoroscopeDaily.new  
  end

  def edit
  	@horoscope_daily = HoroscopeDaily.find(params[:id])
    get_zodiac
  end

  def create
  	@horoscope_daily = HoroscopeDaily.new(params[:horoscope_daily])

  	respond_to do |format|
  		if @horoscope_daily.save
  			format.html { redirect_to horoscope_dailies_path, notice: 'Horoscope daily was successfully created.' }       
  		else
      	# get_zodiac
      	format.html { render action: "new" }
      end
    end
  end

  
  def update
  	@horoscope_daily = HoroscopeDaily.find(params[:id])   
  	if @horoscope_daily.update_attributes(params[:horoscope_daily])
  		redirect_to horoscope_dailies_path, notice: 'Horoscope daily was successfully updated.'
  	else
  		render action: "edit"
  	end 
  end

  def destroy
  	@horoscope_daily = HoroscopeDaily.find(params[:id])
  	@horoscope_daily.destroy

  	redirect_to horoscope_dailies_url

  end

  # To publish the array of horoscope 
  def publish_unpub_horoscope
  	arr_ids = params[:publish].split(',')
  	@published_horoscope_dailies = HoroscopeDaily.where("id IN (?)", arr_ids)
  	@published_horoscope_dailies.update_all(published: params[:b_val] )

  	params[:date] ||= @published_horoscope_dailies.first.date
  	@horoscope_dailies = HoroscopeDaily.all_zodiac_by_date(params[:date])
  	render :partial => 'indexform'

  end

  def zodiac_names
    date = params[:date].to_date
    horoscope_dailies = HoroscopeDaily.where(date: date)
    ids = horoscope_dailies.collect(&:zodiac_id)
    ids.delete(params[:z_id].to_i) if ids.present? and params[:z_id].present? and params[:h_id].present? and horoscope_dailies.collect(&:id).include?(params[:h_id].to_i)    
    @zodiacs = ids.blank? ? Zodiac.all : Zodiac.where("id NOT IN (?)", ids)
    
  	render :partial => 'shared/zodiacs'
  end

  private

  def get_zodiac
  	@zodiacs = HoroscopeDaily.get_zodiac( {date: params[:date]} , @horoscope_daily)
  end

end
