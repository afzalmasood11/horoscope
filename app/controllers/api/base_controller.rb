module Api

  class BaseController < ActionController::Base
    
    private

    def restrict_access
    	device = ''
    	if params[:token] and params[:api_key] == $APP_SETTING[:api_key]
       	device = Device.find_by_token(params[:token])
      end      
      head :unauthorized unless(device.present? and (((device.valid_upto - DateTime.now )/1.day) > 1) )
    end

    def horoscope_restrict_access
    	device = ''
    	if params[:token] and params[:api_key] == $APP_SETTING[:api_key]
      	device = Device.find_by_token(params[:token])
      end
      # head :unauthorized unless(device.present? and (((device.valid_upto - DateTime.now )/1.day) > 1) )
      head :unauthorized unless(device.present? and ( (device.horoscope_validity - Date.today) > 1 ) )
    end
  
  	def rest_of_horoscope_restrict_access
  		device = ''
    	if params[:token] and params[:api_key] == $APP_SETTING[:api_key]
       device = Device.find_by_token(params[:token])
      end      
      # head :unauthorized unless(device.present? and (((device.valid_upto - DateTime.now )/1.day) > 1) )      
      head :unauthorized unless(device.present? and ( (device.rest_of_horoscope_validity - DateTime.now ) > 1) )
    end

  end
  
end
