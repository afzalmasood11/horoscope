class Api::HoroscopesController < Api::BaseController
		
		before_filter :horoscope_restrict_access

		respond_to :json
		def index
			 respond_with Api::Horoscope::response_builder
		end

	end #end of class
