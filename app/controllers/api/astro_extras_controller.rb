class Api::AstroExtrasController < Api::BaseController

  before_filter :rest_of_horoscope_restrict_access

  respond_to :json

  def index
    date = Time.now.to_date
    season = Season.select('id').where("date_from <= ? and date_to >= ?", date, date)

    return respond_with AstroExtra.where(season_id: season) if season.present?
    respond_with "No season found."
  end

end
