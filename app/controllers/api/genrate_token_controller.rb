module Api
	
	class GenrateTokenController < Api::BaseController
		
    respond_to :json
		class Device < ::Device
			def as_json(options={})
				# super(:only => [:token, :valid_days])
				{ token: self.token,
					valid_days: 
					{
						horoscope: ((self.horoscope_validity.to_date - Date.today).to_i),
						rest_of_horoscope: (self.rest_of_horoscope_validity.to_date - Date.today).to_i
					}
				}
			end
		end 

		def token
			if params[:device_id] and params[:api_key] and params[:api_key] == $APP_SETTING[:api_key]
				device = Device.where(device_id: (params[:device_id])).first
				if device
					respond_with (device.valid_upto > Time.now and device.is_active) ? device : {status: "Invalid token or device."}					
        else
          respond_with Device.create(device_id: params[:device_id], valid_upto: (Time.now + 30.days), horoscope_validity: (Time.now + 30.days), rest_of_horoscope_validity: (Time.now + 30.days) )
        end
      else
        respond_with status: "invalid parameters."
      end
    end

  end #class end

end #module end