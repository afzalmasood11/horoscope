class Api::ValidationsController < Api::BaseController
  before_filter :restrict_access
  
  def iap
    if iphone_valid_params?
    	@device = get_device
      return render json: { status: 'device_id is not valid' } if @device.blank?
      begin
        receipt = Itunes::Receipt.verify! params[:receipt_data], :allow_sandbox_receipt
        pro_id = receipt.product_id
        options = (params[:is_sandbox].to_s == "true") ? {sandbox_id: pro_id} : {production_id: pro_id}
        @in_app_purchase = InAppPurchase.where(options).first
        return render json: { status: 'invalid' } unless @in_app_purchase.present?
        return render json: { status: 'invalid' } unless (@in_app_purchase.module_name == params[:purchase_module].to_s)
        @device.update_attributes(get_params_hash)
        log = create_log
        return render json: { status: 'invalid' } unless log.present?
        return render json: { status: 'valid' }
      rescue StandardError => e
        return render json:  { status: 'invalid'}
      end
    end
    render json:  { status: "invalid parameters." }
  end

  def voucher
  	if voucher_valid_params?
      device = get_device
      return render json: { status: 'device_id is not valid' } if device.blank?
      voucher = Voucher.where(code: params[:voucher], is_active: true).first
      @device_voucher = DeviceVoucher.where("device_id = ? AND voucher_id = ?", device.device_id.to_s, voucher.id.to_s ).first if voucher.present?
      return render json: { status: 'invalid' } if voucher.blank? or @device_voucher
      DeviceVoucher.create(device_id: device.device_id, voucher_id: voucher.id)
      update_device_validity(device, voucher)
      # device.update_attributes(valid_upto: (device.valid_upto + voucher.valid_days.days), is_active: true)
      return render json: { status: 'valid', valid_days: voucher.valid_days }
    end
    render json:  { status: "invalid parameters." }
  end

  private
  
  def iphone_valid_params?
    # params[:receipt_data].present? and ( params[:purchase_module].present? and Device::MODULES_NAME.include?(params[:purchase_module]) )
    params[:is_sandbox].present? and params[:receipt_data].present? and params[:purchase_module].present?
  end
  # def iphone_valid_params?
  #   params[:receipt_data] and ( params[:horoscope_validity] or params[:rest_of_horoscope_validity] )  and params[:device_id]
  # end

  def voucher_valid_params?
    params[:voucher].present?
  end


  def get_params_hash
    params_hash = {}
    #params_hash = {valid_days: 36520, valid_upto: (Time.now + 36520.days), is_active: true } if @device.valid_days < 36520 
    params_hash = {valid_days: 36520, horoscope_validity: (Time.now + 36520.days), is_active: true } if @device.valid_days < 36520 
    # params_hash.merge!({"#{params[:purchase_module]}_validity" =>  true })
    params_hash
  end
  
  def get_device
    Device.where(token: params[:token]).first
  end

  def create_log
    device_iap = DevicesIap.create(device_id: @device.id, in_app_purchase_id: @in_app_purchase.id, req_time: Time.now, receipt_data: params[:receipt_data], status: true, expire_at: (Time.now + 36520.days))
    device_iap.save
  end

  def update_device_validity(device, voucher)
    options = {is_active: true}

    horo_validity = device.horoscope_validity.to_date
    rest_of_horo_validity = device.rest_of_horoscope_validity.to_date
    
    options.merge!(horoscope_validity: ( ((horo_validity - Date.today ).to_i) > 0 ) ? ( horo_validity + voucher.valid_days.days) : (Time.now + voucher.valid_days.days) ) if((horo_validity - Date.today) < 36520 )
    options.merge!(rest_of_horoscope_validity: ( ((rest_of_horo_validity - Date.today ).to_i) > 0 ) ? ( rest_of_horo_validity + voucher.valid_days.days) : (Time.now + voucher.valid_days.days) ) if((rest_of_horo_validity - Date.today) < 36520 )
    
    device.update_attributes(options)
  end
  
  def receipt_data
    "ewoJInNpZ25hdHVyZSIgPSAiQWdMd3hTalFHZmlaZy9PcHVIcDRxWThjd1ZwMFlz
OGg3WWZIc1B3QlhycmVBQjNhQkJqazdIRmVWV3dHTWJBUmQweUwyNURZUTBjNlBS
VnRBMHZncXZMZmZXVUF5NTlXdGwyaFdTSXc2T2Ezc1BwamlnSjRUUVlMY3R6R2Qz
MnMxK3lWaElUS1ZNeTlJT284elR2aEo0Rm9DektBSVdiQzJHVkJCZGJzVTlFeUFB
QURWekNDQTFNd2dnSTdvQU1DQVFJQ0NHVVVrVTNaV0FTMU1BMEdDU3FHU0liM0RR
RUJCUVVBTUg4eEN6QUpCZ05WQkFZVEFsVlRNUk13RVFZRFZRUUtEQXBCY0hCc1pT
QkpibU11TVNZd0pBWURWUVFMREIxQmNIQnNaU0JEWlhKMGFXWnBZMkYwYVc5dUlF
RjFkR2h2Y21sMGVURXpNREVHQTFVRUF3d3FRWEJ3YkdVZ2FWUjFibVZ6SUZOMGIz
SmxJRU5sY25ScFptbGpZWFJwYjI0Z1FYVjBhRzl5YVhSNU1CNFhEVEE1TURZeE5U
SXlNRFUxTmxvWERURTBNRFl4TkRJeU1EVTFObG93WkRFak1DRUdBMVVFQXd3YVVI
VnlZMmhoYzJWU1pXTmxhWEIwUTJWeWRHbG1hV05oZEdVeEd6QVpCZ05WQkFzTUVr
RndjR3hsSUdsVWRXNWxjeUJUZEc5eVpURVRNQkVHQTFVRUNnd0tRWEJ3YkdVZ1NX
NWpMakVMTUFrR0ExVUVCaE1DVlZNd2daOHdEUVlKS29aSWh2Y05BUUVCQlFBRGdZ
MEFNSUdKQW9HQkFNclJqRjJjdDRJclNkaVRDaGFJMGc4cHd2L2NtSHM4cC9Sd1Yv
cnQvOTFYS1ZoTmw0WElCaW1LalFRTmZnSHNEczZ5anUrK0RyS0pFN3VLc3BoTWRk
S1lmRkU1ckdYc0FkQkVqQndSSXhleFRldngzSExFRkdBdDFtb0t4NTA5ZGh4dGlJ
ZERnSnYyWWFWczQ5QjB1SnZOZHk2U01xTk5MSHNETHpEUzlvWkhBZ01CQUFHamNq
QndNQXdHQTFVZEV3RUIvd1FDTUFBd0h3WURWUjBqQkJnd0ZvQVVOaDNvNHAyQzBn
RVl0VEpyRHRkREM1RllRem93RGdZRFZSMFBBUUgvQkFRREFnZUFNQjBHQTFVZERn
UVdCQlNwZzRQeUdVakZQaEpYQ0JUTXphTittVjhrOVRBUUJnb3Foa2lHOTJOa0Jn
VUJCQUlGQURBTkJna3Foa2lHOXcwQkFRVUZBQU9DQVFFQUVhU2JQanRtTjRDL0lC
M1FFcEszMlJ4YWNDRFhkVlhBZVZSZVM1RmFaeGMrdDg4cFFQOTNCaUF4dmRXLzNl
VFNNR1k1RmJlQVlMM2V0cVA1Z204d3JGb2pYMGlreVZSU3RRKy9BUTBLRWp0cUIw
N2tMczlRVWU4Y3pSOFVHZmRNMUV1bVYvVWd2RGQ0TndOWXhMUU1nNFdUUWZna1FR
Vnk4R1had1ZIZ2JFL1VDNlk3MDUzcEdYQms1MU5QTTN3b3hoZDNnU1JMdlhqK2xv
SHNTdGNURXFlOXBCRHBtRzUrc2s0dHcrR0szR01lRU41LytlMVFUOW5wL0tsMW5q
K2FCdzdDMHhzeTBiRm5hQWQxY1NTNnhkb3J5L0NVdk02Z3RLc21uT09kcVRlc2Jw
MGJzOHNuNldxczBDOWRnY3hSSHVPTVoydG04bnBMVW03YXJnT1N6UT09IjsKCSJw
dXJjaGFzZS1pbmZvIiA9ICJld29KSW05eWFXZHBibUZzTFhCMWNtTm9ZWE5sTFdS
aGRHVXRjSE4wSWlBOUlDSXlNREV6TFRBNUxUQTVJREEyT2pReU9qRTJJRUZ0WlhK
cFkyRXZURzl6WDBGdVoyVnNaWE1pT3dvSkluVnVhWEYxWlMxcFpHVnVkR2xtYVdW
eUlpQTlJQ0l3TURBd1lqQXlNVGc0TVRnaU93b0pJbTl5YVdkcGJtRnNMWFJ5WVc1
ellXTjBhVzl1TFdsa0lpQTlJQ0l4TURBd01EQXdNRGcyTmpJMk1EUXdJanNLQ1NK
aWRuSnpJaUE5SUNJeExqQWlPd29KSW5SeVlXNXpZV04wYVc5dUxXbGtJaUE5SUNJ
eE1EQXdNREF3TURnMk5qSTJNRFF3SWpzS0NTSnhkV0Z1ZEdsMGVTSWdQU0FpTVNJ
N0Nna2liM0pwWjJsdVlXd3RjSFZ5WTJoaGMyVXRaR0YwWlMxdGN5SWdQU0FpTVRN
M09EY3pOREV6TmpFNU1TSTdDZ2tpZFc1cGNYVmxMWFpsYm1SdmNpMXBaR1Z1ZEds
bWFXVnlJaUE5SUNJMk5FRkRNa0V4T1MweU16TkRMVFF4TWpBdFFqWkROQzFCTVRV
NE9FVXlRa1k0UkRjaU93b0pJbkJ5YjJSMVkzUXRhV1FpSUQwZ0ltUmxMbU52WlhW
ekxuTjFZaklpT3dvSkltbDBaVzB0YVdRaUlEMGdJamN3TWpneU5qVXhOeUk3Q2dr
aVltbGtJaUE5SUNKa1pTNWpiMlYxY3k1c1pYZGxZaUk3Q2draWNIVnlZMmhoYzJV
dFpHRjBaUzF0Y3lJZ1BTQWlNVE0zT0Rjek5ERXpOakU1TVNJN0Nna2ljSFZ5WTJo
aGMyVXRaR0YwWlNJZ1BTQWlNakF4TXkwd09TMHdPU0F4TXpvME1qb3hOaUJGZEdN
dlIwMVVJanNLQ1NKd2RYSmphR0Z6WlMxa1lYUmxMWEJ6ZENJZ1BTQWlNakF4TXkw
d09TMHdPU0F3TmpvME1qb3hOaUJCYldWeWFXTmhMMHh2YzE5QmJtZGxiR1Z6SWpz
S0NTSnZjbWxuYVc1aGJDMXdkWEpqYUdGelpTMWtZWFJsSWlBOUlDSXlNREV6TFRB
NUxUQTVJREV6T2pReU9qRTJJRVYwWXk5SFRWUWlPd3A5IjsKCSJlbnZpcm9ubWVu
dCIgPSAiU2FuZGJveCI7CgkicG9kIiA9ICIxMDAiOwoJInNpZ25pbmctc3RhdHVz
IiA9ICIwIjsKfQ=="
  end
  
end
