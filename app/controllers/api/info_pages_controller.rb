# info_pages_controller.rb
class Api::InfoPagesController < Api::BaseController
  
  before_filter :rest_of_horoscope_restrict_access
  
  respond_to :json

  def info		
    respond_with InformationPage.all			
  end
end