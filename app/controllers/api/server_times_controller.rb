# server_times_controller.rb

class Api::ServerTimesController < Api::BaseController	

  respond_to :json

	def get_server_time
		respond_with Time.now.to_i

	end

end
