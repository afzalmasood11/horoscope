class Api::VideosController < Api::BaseController

  before_filter :rest_of_horoscope_restrict_access

  respond_to :json

  def get_videos
  	respond_with Video.where(is_active: true)
  end

end