class Api::UserLexiconsController < Api::BaseController
	
	before_filter :rest_of_horoscope_restrict_access

	respond_to :json

	def create_user_lexicon
		if question_validate_params?
			user_lexicon = UserLexicon.new(date: DateTime.now.strftime("%Y-%m-%d %H:%M:%S"), user: params[:name], email: params[:email_id], issue: params[:question], is_active: false)
			if user_lexicon.save
				return respond_with({status: "valid"}, location: nil)
			end
		end
		respond_with({status: "Invalid"}, location: nil)
	end

	def answers
		respond_with UserLexicon.where('is_active =? AND reply IS NOT NULL AND reply <> ?', true, '')
	end

	private

	def question_validate_params?
		params[:name].present? and params[:email_id].present? and params[:question].present?
	end

end