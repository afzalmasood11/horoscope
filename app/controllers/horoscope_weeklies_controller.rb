class HoroscopeWeekliesController < ApplicationController
  
  before_filter :date_validator
  before_filter :get_zodiac, only: :new

  def index  
    params[:date] = Date.today if params[:date].blank?
    @horoscope_weeklies = HoroscopeWeekly.all_zodiac_by_date(params[:date])

    render partial: 'indexform' if request.xhr?    
  end

  def show
    @horoscope_weekly = HoroscopeWeekly.find(params[:id])
  end

  def new
    @horoscope_weekly = HoroscopeWeekly.new  
  end

  def edit
    @horoscope_weekly = HoroscopeWeekly.find(params[:id])
    get_zodiac
  end

  def create
    @horoscope_weekly = HoroscopeWeekly.new(params[:horoscope_weekly])

    respond_to do |format|
      if @horoscope_weekly.save
        format.html { redirect_to horoscope_weeklies_path, notice: 'Horoscope weekly was successfully created.' }
        format.json { render json: @horoscope_weekly, status: :created, location: @horoscope_weekly }
      else
        format.html { render action: "new" }
        format.json { render json: @horoscope_weekly.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @horoscope_weekly = HoroscopeWeekly.find(params[:id])

    respond_to do |format|
      if @horoscope_weekly.update_attributes(params[:horoscope_weekly])
        format.html { redirect_to horoscope_weeklies_path, notice: 'Horoscope weekly was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @horoscope_weekly.errors, status: :unprocessable_entity }
      end
    end
  end
  
 
  
  # To publish the array of horoscope 
  def publish_unpub_horoscope
    id = params[:publish].split(',')
    @published_horoscope_weeklies = HoroscopeWeekly.where("id IN (?)", id)
    @published_horoscope_weeklies.update_all(published: params[:b_val] )   
    
    params[:date] ||= @published_horoscope_weeklies.first.date
    @horoscope_weeklies = HoroscopeWeekly.all_zodiac_by_date(params[:date])
    render :partial => 'indexform'    
  end
  
  
  def destroy
    @horoscope_weekly = HoroscopeWeekly.find(params[:id])
    @horoscope_weekly.destroy

    respond_to do |format|
      format.html { redirect_to horoscope_weeklies_url }
      format.json { head :no_content }
    end
  end

  def zodiac_names
    given_date = params[:date].to_date
    # horoscope_weeklies = HoroscopeWeekly.by_week(given_date.strftime("%W").to_i, year: given_date.year, field: date)
    horoscope_weeklies = HoroscopeWeekly.by_given_date(given_date)
    ids = horoscope_weeklies.collect(&:zodiac_id)
    ids.delete(params[:z_id].to_i) if ids.present? and params[:z_id].present? and params[:h_id].present? and horoscope_weeklies.collect(&:id).include?(params[:h_id].to_i)
    @zodiacs = ids.blank? ? Zodiac.all : Zodiac.where("id NOT IN (?)", ids)

    render :partial => 'shared/zodiacs'
  end

  private

  def get_zodiac
    @zodiacs = HoroscopeWeekly.zodiac_list(params[:date] , @horoscope_weekly)
    if @zodiacs.blank?
      redirect_to horoscope_weeklies_path, notice: 'All zodiacs for this week has created.'
    end
  end

end
