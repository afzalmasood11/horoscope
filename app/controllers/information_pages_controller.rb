class InformationPagesController < ApplicationController
  # GET /information_pages
  # GET /information_pages.json
  def index
    @information_pages = InformationPage.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @information_pages }
    end
  end

  # GET /information_pages/1
  # GET /information_pages/1.json
  def show
    @information_page = InformationPage.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @information_page }
    end
  end

  # GET /information_pages/new
  # GET /information_pages/new.json
  def new
    @information_page = InformationPage.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @information_page }
    end
  end

  # GET /information_pages/1/edit
  def edit
    @information_page = InformationPage.find(params[:id])
  end

  # POST /information_pages
  # POST /information_pages.json
  def create
    @information_page = InformationPage.new(params[:information_page])

    respond_to do |format|
      if @information_page.save
        format.html { redirect_to @information_page, notice: 'Information page was successfully created.' }
        format.json { render json: @information_page, status: :created, location: @information_page }
      else
        format.html { render action: "new" }
        format.json { render json: @information_page.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /information_pages/1
  # PUT /information_pages/1.json
  def update
    @information_page = InformationPage.find(params[:id])

    respond_to do |format|
      if @information_page.update_attributes(params[:information_page])
        format.html { redirect_to @information_page, notice: 'Information page was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @information_page.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /information_pages/1
  # DELETE /information_pages/1.json
  def destroy
    @information_page = InformationPage.find(params[:id])
    @information_page.destroy

    respond_to do |format|
      format.html { redirect_to information_pages_url }
      format.json { head :no_content }
    end
  end
end
