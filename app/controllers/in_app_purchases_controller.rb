class InAppPurchasesController < ApplicationController
  
  def index
    @in_app_purchases = InAppPurchase.all
  end

  def show
    @in_app_purchase = InAppPurchase.find(params[:id])
  end

  def new
    @in_app_purchase = InAppPurchase.new
  end

  def edit
    @in_app_purchase = InAppPurchase.find(params[:id])
  end

  def create
    @in_app_purchase = InAppPurchase.new(params[:in_app_purchase])

    respond_to do |format|
      if @in_app_purchase.save
        format.html { redirect_to in_app_purchases_path, notice: 'In app purchase was successfully created.' }
        format.json { render json: @in_app_purchase, status: :created, location: @in_app_purchase }
      else
        format.html { render action: "new" }
        format.json { render json: @in_app_purchase.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @in_app_purchase = InAppPurchase.find(params[:id])

    respond_to do |format|
      if @in_app_purchase.update_attributes(params[:in_app_purchase])
        format.html { redirect_to in_app_purchases_path, notice: 'In app purchase was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @in_app_purchase.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @in_app_purchase = InAppPurchase.find(params[:id])
    @in_app_purchase.destroy

    respond_to do |format|
      format.html { redirect_to in_app_purchases_url }
      format.json { head :no_content }
    end
  end
end
