class ApplicationController < ActionController::Base
  protect_from_forgery
  # before_filter :authenticate_user!
  before_filter :authenticator
   rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_path, :alert => exception.message
  end

  private

  def authenticator
  	authenticate_user! unless  Rails.env.test?
  end

  def date_validator
    begin
      params[:date].present? and params[:date].to_date
    rescue
      flash[:error] = 'Date must be a valid'
      redirect_to :back
    end
  end
  
end
