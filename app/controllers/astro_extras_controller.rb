class AstroExtrasController < ApplicationController

  before_filter :get_seasons
	before_filter :get_zodiac, except: [:destroy, :edit]
  
  def index
    get_astro_extras    
  end

	def show
    @astro_extra = AstroExtra.find(params[:id])
  end

  def new
  	@astro_extra = AstroExtra.new
  	get_zodiac(@seasons.first[1])
  end

  def edit
    @astro_extra = AstroExtra.find(params[:id])
    get_zodiac
  end

  def create
    @astro_extra = AstroExtra.new(params[:astro_extra])

    respond_to do |format|
      if @astro_extra.save
        format.html { redirect_to astro_extras_path, notice: 'Astro extra was successfully created.' }
        format.json { render json: @astro_extra, status: :created, location: @astro_extra }
      else
        format.html { render action: "new" }
        format.json { render json: @astro_extra.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @astro_extra = AstroExtra.find(params[:id])

    respond_to do |format|
      if @astro_extra.update_attributes(params[:astro_extra])
        format.html { redirect_to astro_extras_path, notice: 'Astro extra was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @astro_extra.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @astro_extra = AstroExtra.find(params[:id])
    @astro_extra.destroy

    respond_to do |format|
      format.html { redirect_to astro_extras_url }
      format.json { head :no_content }
    end
  end

  # To publish astro_extra
  def pub_unpub_astro_extra
  	
  	uri = URI.parse(request.headers["Referer"])

  	parameters = {:page => 1}
  	parameters[:page] =  CGI.parse(uri.query)['page'].first if uri.query.present?
  	params.merge!(parameters)


  	astro_extra = AstroExtra.find(params[:id])
  	astro_extra.update_attributes(is_active: params[:is_active] ) 	
  	get_astro_extras

  	render :partial => 'astro_extras'

  end

  def zodiac_names  	
  	render :partial => 'shared/zodiacs'
  end

  private

  def get_astro_extras
  	@astro_extras = AstroExtra.order('id').paginate(page: params[:page]).includes([:zodiac, :season])
  end

  def get_zodiac(season_id=nil)
  	params[:season_id] ||= season_id.to_s if season_id.present?
  	@zodiacs = AstroExtra.get_zodiac({season_id: params[:season_id]}, @astro_extra)
  end

  def get_seasons
  	@seasons = AstroExtra.get_seasons
  	
  	if @seasons.empty?
  		redirect_to new_season_path, notice: "First create atleast one season."
  	end
  end

end
