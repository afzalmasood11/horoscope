class UserLexiconsController < ApplicationController

  load_and_authorize_resource
  
  def index
    get_user_lexicons
  end

  def show
    @user_lexicon = UserLexicon.find(params[:id])
  end

  # def new
  #   @user_lexicon = UserLexicon.new
  # end

  def edit
    @user_lexicon = UserLexicon.find(params[:id])
  end

  # def create
  #   @user_lexicon = UserLexicon.new(params[:user_lexicon])

  #   respond_to do |format|
  #     if @user_lexicon.save
  #       format.html { redirect_to user_lexicons_path, notice: 'User lexicon was successfully created.' }
  #       format.json { render json: @user_lexicon, status: :created, location: @user_lexicon }
  #     else
  #       format.html { render action: "new" }
  #       format.json { render json: @user_lexicon.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  def update
    @user_lexicon = UserLexicon.find(params[:id])

    respond_to do |format|
      if @user_lexicon.update_attributes(params[:user_lexicon])
        format.html { redirect_to user_lexicons_path, notice: 'User lexicon was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @user_lexicon.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @user_lexicon = UserLexicon.find(params[:id])
    @user_lexicon.destroy

    respond_to do |format|
      format.html { redirect_to user_lexicons_url }
      format.json { head :no_content }
    end
  end


  # To publish user_lexicon 
  def pub_unpub_user_lexicon 
    
    uri = URI.parse(request.headers["Referer"])

    parameters = {:page => 1}
    parameters[:page] =  CGI.parse(uri.query)['page'].first if uri.query.present?
    params.merge!(parameters)
    user_lexicon = UserLexicon.find(params[:id])
    return render nothing: true, status: 500 if user_lexicon.reply.blank?
    user_lexicon.update_attributes(is_active: params[:is_active] )   
    get_user_lexicons

    render :partial => 'user_lexicons'

  end

  private
  def get_user_lexicons
    @user_lexicons = UserLexicon.order('id').paginate(page: params[:page])   
  end

end
