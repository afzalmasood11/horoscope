require "spec_helper"

describe HoroscopeWeekliesController do
  describe "routing" do

    it "routes to #index" do
      get("/horoscope_weeklies").should route_to("horoscope_weeklies#index")
    end

    it "routes to #new" do
      get("/horoscope_weeklies/new").should route_to("horoscope_weeklies#new")
    end

    it "routes to #show" do
      get("/horoscope_weeklies/1").should route_to("horoscope_weeklies#show", :id => "1")
    end

    it "routes to #edit" do
      get("/horoscope_weeklies/1/edit").should route_to("horoscope_weeklies#edit", :id => "1")
    end

    it "routes to #create" do
      post("/horoscope_weeklies").should route_to("horoscope_weeklies#create")
    end

    it "routes to #update" do
      put("/horoscope_weeklies/1").should route_to("horoscope_weeklies#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/horoscope_weeklies/1").should route_to("horoscope_weeklies#destroy", :id => "1")
    end

  end
end
