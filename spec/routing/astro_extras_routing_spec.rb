require "spec_helper"

describe AstroExtrasController do
  describe "routing" do

    it "routes to #index" do
      get("/astro_extras").should route_to("astro_extras#index")
    end

    it "routes to #new" do
      get("/astro_extras/new").should route_to("astro_extras#new")
    end

    it "routes to #show" do
      get("/astro_extras/1").should route_to("astro_extras#show", :id => "1")
    end

    it "routes to #edit" do
      get("/astro_extras/1/edit").should route_to("astro_extras#edit", :id => "1")
    end

    it "routes to #create" do
      post("/astro_extras").should route_to("astro_extras#create")
    end

    it "routes to #update" do
      put("/astro_extras/1").should route_to("astro_extras#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/astro_extras/1").should route_to("astro_extras#destroy", :id => "1")
    end

  end
end
