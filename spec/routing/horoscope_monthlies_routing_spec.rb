require "spec_helper"

describe HoroscopeMonthliesController do
  describe "routing" do

    it "routes to #index" do
      get("/horoscope_monthlies").should route_to("horoscope_monthlies#index")
    end

    it "routes to #new" do
      get("/horoscope_monthlies/new").should route_to("horoscope_monthlies#new")
    end

    it "routes to #show" do
      get("/horoscope_monthlies/1").should route_to("horoscope_monthlies#show", :id => "1")
    end

    it "routes to #edit" do
      get("/horoscope_monthlies/1/edit").should route_to("horoscope_monthlies#edit", :id => "1")
    end

    it "routes to #create" do
      post("/horoscope_monthlies").should route_to("horoscope_monthlies#create")
    end

    it "routes to #update" do
      put("/horoscope_monthlies/1").should route_to("horoscope_monthlies#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/horoscope_monthlies/1").should route_to("horoscope_monthlies#destroy", :id => "1")
    end

  end
end
