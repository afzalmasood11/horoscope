require "spec_helper"

describe HoroscopeDailiesController do
  describe "routing" do

    it "routes to #index" do
      get("/horoscope_dailies").should route_to("horoscope_dailies#index")
    end

    it "routes to #new" do
      get("/horoscope_dailies/new").should route_to("horoscope_dailies#new")
    end

    it "routes to #show" do
      get("/horoscope_dailies/1").should route_to("horoscope_dailies#show", :id => "1")
    end

    it "routes to #edit" do
      get("/horoscope_dailies/1/edit").should route_to("horoscope_dailies#edit", :id => "1")
    end

    it "routes to #create" do
      post("/horoscope_dailies").should route_to("horoscope_dailies#create")
    end

    it "routes to #update" do
      put("/horoscope_dailies/1").should route_to("horoscope_dailies#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/horoscope_dailies/1").should route_to("horoscope_dailies#destroy", :id => "1")
    end

  end
end
