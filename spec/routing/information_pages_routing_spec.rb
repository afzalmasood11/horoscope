require "spec_helper"

describe InformationPagesController do
  describe "routing" do

    it "routes to #index" do
      get("/information_pages").should route_to("information_pages#index")
    end

    it "routes to #new" do
      get("/information_pages/new").should route_to("information_pages#new")
    end

    it "routes to #show" do
      get("/information_pages/1").should route_to("information_pages#show", :id => "1")
    end

    it "routes to #edit" do
      get("/information_pages/1/edit").should route_to("information_pages#edit", :id => "1")
    end

    it "routes to #create" do
      post("/information_pages").should route_to("information_pages#create")
    end

    it "routes to #update" do
      put("/information_pages/1").should route_to("information_pages#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/information_pages/1").should route_to("information_pages#destroy", :id => "1")
    end

  end
end
