require "spec_helper"

describe UserLexiconsController do
  describe "routing" do

    it "routes to #index" do
      get("/user_lexicons").should route_to("user_lexicons#index")
    end

    it "routes to #new" do
      get("/user_lexicons/new").should route_to("user_lexicons#new")
    end

    it "routes to #show" do
      get("/user_lexicons/1").should route_to("user_lexicons#show", :id => "1")
    end

    it "routes to #edit" do
      get("/user_lexicons/1/edit").should route_to("user_lexicons#edit", :id => "1")
    end

    it "routes to #create" do
      post("/user_lexicons").should route_to("user_lexicons#create")
    end

    it "routes to #update" do
      put("/user_lexicons/1").should route_to("user_lexicons#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/user_lexicons/1").should route_to("user_lexicons#destroy", :id => "1")
    end

  end
end
