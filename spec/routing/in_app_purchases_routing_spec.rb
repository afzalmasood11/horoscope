require "spec_helper"

describe InAppPurchasesController do
  describe "routing" do

    it "routes to #index" do
      get("/in_app_purchases").should route_to("in_app_purchases#index")
    end

    it "routes to #new" do
      get("/in_app_purchases/new").should route_to("in_app_purchases#new")
    end

    it "routes to #show" do
      get("/in_app_purchases/1").should route_to("in_app_purchases#show", :id => "1")
    end

    it "routes to #edit" do
      get("/in_app_purchases/1/edit").should route_to("in_app_purchases#edit", :id => "1")
    end

    it "routes to #create" do
      post("/in_app_purchases").should route_to("in_app_purchases#create")
    end

    it "routes to #update" do
      put("/in_app_purchases/1").should route_to("in_app_purchases#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/in_app_purchases/1").should route_to("in_app_purchases#destroy", :id => "1")
    end

  end
end
