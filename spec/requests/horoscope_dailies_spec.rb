require 'spec_helper'

describe "HoroscopeDailies" do

	subject {page}

	describe "horoscope dailies page" do

		before { get horoscope_dailies_path }
		it "http response should be 200" do
			response.status.should be(200)
		end

		let(:zodiac_list) { Zodiac.all.collect(&:name).to_set }

		it "should have all zodiacs" do
			zodiac_names = assigns(:horoscope_dailies).collect(&:zodiac_name).to_set
			expect(zodiac_names).to eq(zodiac_list)    	
		end

	end

end
