require 'spec_helper'

describe "AstroExtras" do

	subject{ page }

	let(:index_path){ astro_extras_path }

	before { 
		sign_in(build(:user))
	}

  # describe "GET /astro_extras" do
  #   it "works! (now write some real specs)" do
  #     # Run the generator again with the --webrat flag if you want to use webrat methods/matchers
  #     get astro_extras_path
  #     response.status.should be(200)
  #   end
  # end

  describe "#index" do
		# before(:all){
		# 	20.times { create(:astro_extra, season_id: get_voucher_code, zodiac_id: ) }
		# }
		after(:all){ AstroExtras.delete_all }
		before{ visit index_path }

		it { should have_link("Astro Extras")	}

		context "when no astro extra available" do
			before(:all){ AstroExtras.delete_all }
			it { should have_content('No astro extra available.') }
			it { (AstroExtras.count).should be(0) }
		end

		# context "when voucher(s) available" do

		# 	it "should list each voucher" do
		# 		Voucher.paginate(page: 1).each do |voucher|
		# 			expect(page).to have_selector('td', text: voucher.code)
		# 			params = { id: voucher.id }
		# 			expect(page).to have_link('Show',    href: voucher_path(params) )
		# 			expect(page).to have_link('Edit',    href: edit_voucher_path(params) )
		# 			expect(page).to have_link('Destroy',   href: voucher_path(params) )
		# 			expect(page).to have_link('New Voucher',    href: new_voucher_path)
		# 		end
		# 	end

		# end

		


	end

end
