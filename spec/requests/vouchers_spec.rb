require 'spec_helper'

describe "Vouchers" do

	subject{ page }

	let(:voucher) { create(:voucher) }
	let(:params) { { id: voucher.id } }

	let(:show_path){voucher_path(params)}
	let(:edit_path){edit_voucher_path(params)}
	let(:new_path){ new_voucher_path }
	let(:index_path){ vouchers_path }

	let(:submit) { "Save" }

	before { 
		sign_in(build(:user))
	}

	describe "for authenticate user" do
		before { 
			click_link "Vouchers" 
		}
		it { should have_content("Listing vouchers") }
		it { should have_link('New Voucher',    href: new_path) }

	end

	describe "#new" do

		before { visit new_path }

		context "with invalid information" do
			it "should not create a voucher" do
				expect {click_button submit}.not_to change(Voucher, :count)
			end
			describe "after submition" do
				before { click_button submit }
				it { "#{current_path}/new".should == new_path }
				it { should have_selector('h1', text: 'New voucher') }
				it { should have_content('prohibited this voucher from being saved') }
			end
		end

		context "with valid information" do
			before do
				fill_in "voucher_valid_days",   with: 5
				check "voucher_is_active"
				fill_in 'voucher_code', with: get_voucher_code
			end

			it "should create a voucher" do
				expect { click_button submit }.to change(Voucher, :count).by(1)
			end

			describe "after submition" do
				before { click_button submit }
				let(:show_path){voucher_path(id: Voucher.last.id)}
				it { current_path.should == show_path }
				it { should have_content('Voucher was successfully created.') }	      
			end     
		end
	end

	describe "#edit" do

		before{ visit edit_path }

		context "with invalid information" do
			before{ fill_in "voucher_valid_days",   with: -1 }
			it "should not update a voucher" do
				expect {click_button submit}.not_to change(Voucher, :count)
			end

			describe "after submition" do
				before{ click_button submit }
				it { "#{current_path}/edit".should == edit_path }
				it { should have_selector('h1', text: 'Editing voucher') }
				it { should have_content('prohibited this voucher from being saved') }
			end
		end

		context "with valid information" do
			let(:new_days) { 10 }
			let(:new_voucher_code) { get_voucher_code }

			before do
				fill_in "voucher_valid_days",   with: new_days
				check "voucher_is_active"
				fill_in 'voucher_code', with: new_voucher_code
				click_button submit
			end

			specify { expect(voucher.reload.valid_days).to  eq new_days }
			specify { expect(voucher.reload.code).to eq(new_voucher_code) }

			describe "after submission" do
				it { current_path.should == show_path }
				it { should have_content('Voucher was successfully updated.') }
			end

		end
	end

	describe "#index" do
		before(:all){ 
			20.times { create(:voucher, code: get_voucher_code) }			
		}
		after(:all){ Voucher.delete_all }
		before{ visit index_path }

		it { should have_link("Vouchers")	}

		context "when voucher(s) available" do

			it "should list each voucher" do
				Voucher.paginate(page: 1).each do |voucher|
					expect(page).to have_selector('td', text: voucher.code)
					params = { id: voucher.id }
					expect(page).to have_link('Show',    href: voucher_path(params) )
					expect(page).to have_link('Edit',    href: edit_voucher_path(params) )
					expect(page).to have_link('Destroy',   href: voucher_path(params) )
					expect(page).to have_link('New Voucher',    href: new_voucher_path)
				end
			end

		end

		context "when no voucher available" do
			before(:all){ Voucher.delete_all }
			it { should have_content('No voucher available.') }
			it { (Voucher.count).should be(0) }
		end


	end

	describe "#delete" do
		before(:all){ 
			20.times { create(:voucher, code: get_voucher_code) }			
		}
		
		let(:destroy_path){voucher_path(Voucher.first)}

		after(:all){ Voucher.delete_all }

		describe "when click destroy link" do
			before {				
				visit index_path 
			}

			it { should have_link('Destroy', href: destroy_path ) }
			it "should delete Voucher" do
				expect do				
					click_link('Destroy', match: :first)
				end.to change(Voucher, :count).by(-1)				
			end
		end

		describe "after destroy voucher", js: true do
			before {
				visit index_path				
				click_link('Destroy', match: :first) 
				page.driver.browser.switch_to.alert.accept
			}			
			it { current_path.should == index_path }
			it { should have_content('Voucher destroyed.') }
			it { should_not have_link('Destroy', href: destroy_path) }
		end

	end


end
