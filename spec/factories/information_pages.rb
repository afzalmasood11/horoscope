# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :information_page do
    name "MyString"
    title "MyString"
    body "MyText"
  end
end
