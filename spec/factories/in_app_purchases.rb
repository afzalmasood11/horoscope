# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :in_app_purchase do
    module_name "MyString"
    sandbox_id "MyString"
    production_id "MyString"
  end
end
