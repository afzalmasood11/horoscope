# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :moon_phase do
    horoscope_monthly_id nil
    phase_name "MyString"
    data "MyText"
    priority 1
  end
end
