# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :device_voucher, :class => 'DeviceVouchers' do
    device_id "MyString"
    voucher_id "MyString"
  end
end
