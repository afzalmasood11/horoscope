	# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :horoscope_daily do

    published false
    date   Date.today
    trend  "test_trend"
    money  "test_money"
    health "test_health"
    love   "test_love"
    tip    "test_tip"

  end

end
