# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :horoscope_monthly do
    zodiac_id 1
    date "2013-09-03"
    published false
    full_moon "MyText"
    quarter_moon_1 "MyText"
    new_moon "MyText"
    quarter_moon_2 "MyText"
    health "MyText"
  end
end
