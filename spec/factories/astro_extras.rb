# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :astro_extra do
    title "MyString"
    horoscope "MyString"
    image "MyString"
    is_active false
  end
end
