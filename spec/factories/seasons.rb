# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :season do
    date_from "MyString"
    date_to "MyString"
  end
end
