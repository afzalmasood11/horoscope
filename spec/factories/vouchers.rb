FactoryGirl.define do

  factory :voucher do
  	code SecureRandom.hex(4).to_s
  	valid_days 10
    is_active false
  end

end