# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
	
  factory :device do

    sequence(:device_id) {|n| "device_#{n}" }
    token SecureRandom.hex
    valid_days 1
    is_active true

  end
end
