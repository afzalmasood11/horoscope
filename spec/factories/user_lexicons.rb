# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user_lexicon do
    date "2013-09-26 14:16:16"
    user "MyString"
    email "MyString"
    issue "MyText"
    reply "MyText"
    is_active false
  end
end
