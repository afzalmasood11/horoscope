# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :devices_iap do
    device nil
    in_app_purchase nil
    req_time "MyString"
    receipt_data "MyText"
    status false
    expire_at "2013-10-03"
  end
end
