require 'spec_helper'

describe HoroscopeDaily do
  
  before { 
  	z_id = Zodiac.first.id
  	@horoscope_daily = FactoryGirl.build(:horoscope_daily, zodiac_id: z_id )
  }

  subject { @horoscope_daily }

  it { should respond_to(:date) }
  it { should respond_to(:published) }
  it { should respond_to(:trend) }
  it { should respond_to(:money) }
  it { should respond_to(:health) }
  it { should respond_to(:love) }
  it { should respond_to(:tip) }

  it { should be_valid }


  describe "when date is not present" do
		before {@horoscope_daily.date = ""}  

		it { should_not be_valid }
  end

  describe "when date is invalid" do
  	before { @horoscope_daily.date = "122bc" }

  	it { should_not be_valid }
  end

  describe "when created, published" do
  	it { expect(@horoscope_daily.published).to be_false }
  end

  describe "when published is not present" do
  	before { @horoscope_daily.published = "" }
  	it { should_not be_valid }
  end

end
