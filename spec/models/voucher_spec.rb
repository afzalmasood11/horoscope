require 'spec_helper'

describe Voucher do
	
  let(:voucher){ build(:voucher) }

  subject { voucher }

  it { should respond_to(:code) }
  it { should respond_to(:valid_days) }
  it { should respond_to(:is_active) }

  it { should be_valid }

  describe "code should be present." do
  	before { voucher.code = nil }

  	it { should_not be_valid }
  end

  describe "when code is invalid" do
  	before { voucher.code = "a" }

  	it { should_not be_valid }
  end

  describe "code shoud be unique" do
		let(:voucher2){ build(:voucher, code: SecureRandom.hex(4).to_s) }			 
		
		it { expect(voucher.code).not_to eq(voucher2.code) }
  end

  describe "when valid_days is invalid" do
  	before { voucher.valid_days = -12 }

  	it { should_not be_valid }
  end

  describe "when created, is_active" do
  	it { expect(voucher.is_active).to be_false }
  end

end