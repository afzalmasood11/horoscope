def hd_test_data(published, z_id)
	horoscopes= []
	(1..3).each do |i|
		horoscopes << create(:horoscope_daily, zodiac_id: z_id, published: published)
	end
	initial = horoscopes.collect(&:published)
	hd_ids= horoscopes.collect(&:id).join(",")
	return horoscopes, initial, hd_ids
end

def pub_unpub_horoscope(published)
	post :publish_unpub_horoscope, {:publish => @hd_ids, :b_val => published, :method => :collection}
end

def get_daily_horoscopes_list(ids_list)
	HoroscopeDaily.where(['id IN (?)', ids_list.split(',')]).collect(&:published)
end

def get_voucher_code
	begin
		voucher_code = SecureRandom.hex(4)
	end while Voucher.exists?(code: voucher_code)
	voucher_code
end

def sign_in(user, options={})
	visit new_user_session_path
	fill_in "user_login",    with: user.email
	fill_in "user_password", with: user.password
	click_button "Sign in"  
end

def get_receipt_data
	"ewoJInNpZ25hdHVyZSIgPSAiQWdMd3hTalFHZmlaZy9PcHVIcDRxWThjd1ZwMFlz
	OGg3WWZIc1B3QlhycmVBQjNhQkJqazdIRmVWV3dHTWJBUmQweUwyNURZUTBjNlBS
	VnRBMHZncXZMZmZXVUF5NTlXdGwyaFdTSXc2T2Ezc1BwamlnSjRUUVlMY3R6R2Qz
	MnMxK3lWaElUS1ZNeTlJT284elR2aEo0Rm9DektBSVdiQzJHVkJCZGJzVTlFeUFB
	QURWekNDQTFNd2dnSTdvQU1DQVFJQ0NHVVVrVTNaV0FTMU1BMEdDU3FHU0liM0RR
	RUJCUVVBTUg4eEN6QUpCZ05WQkFZVEFsVlRNUk13RVFZRFZRUUtEQXBCY0hCc1pT
	QkpibU11TVNZd0pBWURWUVFMREIxQmNIQnNaU0JEWlhKMGFXWnBZMkYwYVc5dUlF
	RjFkR2h2Y21sMGVURXpNREVHQTFVRUF3d3FRWEJ3YkdVZ2FWUjFibVZ6SUZOMGIz
	SmxJRU5sY25ScFptbGpZWFJwYjI0Z1FYVjBhRzl5YVhSNU1CNFhEVEE1TURZeE5U
	SXlNRFUxTmxvWERURTBNRFl4TkRJeU1EVTFObG93WkRFak1DRUdBMVVFQXd3YVVI
	VnlZMmhoYzJWU1pXTmxhWEIwUTJWeWRHbG1hV05oZEdVeEd6QVpCZ05WQkFzTUVr
	RndjR3hsSUdsVWRXNWxjeUJUZEc5eVpURVRNQkVHQTFVRUNnd0tRWEJ3YkdVZ1NX
	NWpMakVMTUFrR0ExVUVCaE1DVlZNd2daOHdEUVlKS29aSWh2Y05BUUVCQlFBRGdZ
	MEFNSUdKQW9HQkFNclJqRjJjdDRJclNkaVRDaGFJMGc4cHd2L2NtSHM4cC9Sd1Yv
	cnQvOTFYS1ZoTmw0WElCaW1LalFRTmZnSHNEczZ5anUrK0RyS0pFN3VLc3BoTWRk
	S1lmRkU1ckdYc0FkQkVqQndSSXhleFRldngzSExFRkdBdDFtb0t4NTA5ZGh4dGlJ
	ZERnSnYyWWFWczQ5QjB1SnZOZHk2U01xTk5MSHNETHpEUzlvWkhBZ01CQUFHamNq
	QndNQXdHQTFVZEV3RUIvd1FDTUFBd0h3WURWUjBqQkJnd0ZvQVVOaDNvNHAyQzBn
	RVl0VEpyRHRkREM1RllRem93RGdZRFZSMFBBUUgvQkFRREFnZUFNQjBHQTFVZERn
	UVdCQlNwZzRQeUdVakZQaEpYQ0JUTXphTittVjhrOVRBUUJnb3Foa2lHOTJOa0Jn
	VUJCQUlGQURBTkJna3Foa2lHOXcwQkFRVUZBQU9DQVFFQUVhU2JQanRtTjRDL0lC
	M1FFcEszMlJ4YWNDRFhkVlhBZVZSZVM1RmFaeGMrdDg4cFFQOTNCaUF4dmRXLzNl
	VFNNR1k1RmJlQVlMM2V0cVA1Z204d3JGb2pYMGlreVZSU3RRKy9BUTBLRWp0cUIw
	N2tMczlRVWU4Y3pSOFVHZmRNMUV1bVYvVWd2RGQ0TndOWXhMUU1nNFdUUWZna1FR
	Vnk4R1had1ZIZ2JFL1VDNlk3MDUzcEdYQms1MU5QTTN3b3hoZDNnU1JMdlhqK2xv
	SHNTdGNURXFlOXBCRHBtRzUrc2s0dHcrR0szR01lRU41LytlMVFUOW5wL0tsMW5q
	K2FCdzdDMHhzeTBiRm5hQWQxY1NTNnhkb3J5L0NVdk02Z3RLc21uT09kcVRlc2Jw
	MGJzOHNuNldxczBDOWRnY3hSSHVPTVoydG04bnBMVW03YXJnT1N6UT09IjsKCSJw
	dXJjaGFzZS1pbmZvIiA9ICJld29KSW05eWFXZHBibUZzTFhCMWNtTm9ZWE5sTFdS
	aGRHVXRjSE4wSWlBOUlDSXlNREV6TFRBNUxUQTVJREEyT2pReU9qRTJJRUZ0WlhK
	cFkyRXZURzl6WDBGdVoyVnNaWE1pT3dvSkluVnVhWEYxWlMxcFpHVnVkR2xtYVdW
	eUlpQTlJQ0l3TURBd1lqQXlNVGc0TVRnaU93b0pJbTl5YVdkcGJtRnNMWFJ5WVc1
	ellXTjBhVzl1TFdsa0lpQTlJQ0l4TURBd01EQXdNRGcyTmpJMk1EUXdJanNLQ1NK
	aWRuSnpJaUE5SUNJeExqQWlPd29KSW5SeVlXNXpZV04wYVc5dUxXbGtJaUE5SUNJ
	eE1EQXdNREF3TURnMk5qSTJNRFF3SWpzS0NTSnhkV0Z1ZEdsMGVTSWdQU0FpTVNJ
	N0Nna2liM0pwWjJsdVlXd3RjSFZ5WTJoaGMyVXRaR0YwWlMxdGN5SWdQU0FpTVRN
	M09EY3pOREV6TmpFNU1TSTdDZ2tpZFc1cGNYVmxMWFpsYm1SdmNpMXBaR1Z1ZEds
	bWFXVnlJaUE5SUNJMk5FRkRNa0V4T1MweU16TkRMVFF4TWpBdFFqWkROQzFCTVRV
	NE9FVXlRa1k0UkRjaU93b0pJbkJ5YjJSMVkzUXRhV1FpSUQwZ0ltUmxMbU52WlhW
	ekxuTjFZaklpT3dvSkltbDBaVzB0YVdRaUlEMGdJamN3TWpneU5qVXhOeUk3Q2dr
	aVltbGtJaUE5SUNKa1pTNWpiMlYxY3k1c1pYZGxZaUk3Q2draWNIVnlZMmhoYzJV
	dFpHRjBaUzF0Y3lJZ1BTQWlNVE0zT0Rjek5ERXpOakU1TVNJN0Nna2ljSFZ5WTJo
	aGMyVXRaR0YwWlNJZ1BTQWlNakF4TXkwd09TMHdPU0F4TXpvME1qb3hOaUJGZEdN
	dlIwMVVJanNLQ1NKd2RYSmphR0Z6WlMxa1lYUmxMWEJ6ZENJZ1BTQWlNakF4TXkw
	d09TMHdPU0F3TmpvME1qb3hOaUJCYldWeWFXTmhMMHh2YzE5QmJtZGxiR1Z6SWpz
	S0NTSnZjbWxuYVc1aGJDMXdkWEpqYUdGelpTMWtZWFJsSWlBOUlDSXlNREV6TFRB
	NUxUQTVJREV6T2pReU9qRTJJRVYwWXk5SFRWUWlPd3A5IjsKCSJlbnZpcm9ubWVu
	dCIgPSAiU2FuZGJveCI7CgkicG9kIiA9ICIxMDAiOwoJInNpZ25pbmctc3RhdHVz
	IiA9ICIwIjsKfQ=="
end