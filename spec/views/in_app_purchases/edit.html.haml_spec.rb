require 'spec_helper'

describe "in_app_purchases/edit" do
  before(:each) do
    @in_app_purchase = assign(:in_app_purchase, stub_model(InAppPurchase,
      :module_name => "MyString",
      :sandbox_id => "MyString",
      :production_id => "MyString"
    ))
  end

  it "renders the edit in_app_purchase form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", in_app_purchase_path(@in_app_purchase), "post" do
      assert_select "input#in_app_purchase_module_name[name=?]", "in_app_purchase[module_name]"
      assert_select "input#in_app_purchase_sandbox_id[name=?]", "in_app_purchase[sandbox_id]"
      assert_select "input#in_app_purchase_production_id[name=?]", "in_app_purchase[production_id]"
    end
  end
end
