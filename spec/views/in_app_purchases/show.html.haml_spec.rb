require 'spec_helper'

describe "in_app_purchases/show" do
  before(:each) do
    @in_app_purchase = assign(:in_app_purchase, stub_model(InAppPurchase,
      :module_name => "Module Name",
      :sandbox_id => "Sandbox",
      :production_id => "Production"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Module Name/)
    rendered.should match(/Sandbox/)
    rendered.should match(/Production/)
  end
end
