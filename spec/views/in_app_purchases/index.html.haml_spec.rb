require 'spec_helper'

describe "in_app_purchases/index" do
  before(:each) do
    assign(:in_app_purchases, [
      stub_model(InAppPurchase,
        :module_name => "Module Name",
        :sandbox_id => "Sandbox",
        :production_id => "Production"
      ),
      stub_model(InAppPurchase,
        :module_name => "Module Name",
        :sandbox_id => "Sandbox",
        :production_id => "Production"
      )
    ])
  end

  it "renders a list of in_app_purchases" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Module Name".to_s, :count => 2
    assert_select "tr>td", :text => "Sandbox".to_s, :count => 2
    assert_select "tr>td", :text => "Production".to_s, :count => 2
  end
end
