require 'spec_helper'

describe "user_lexicons/show" do
  before(:each) do
    @user_lexicon = assign(:user_lexicon, stub_model(UserLexicon,
      :user => "User",
      :email => "Email",
      :issue => "MyText",
      :reply => "MyText",
      :is_active => false
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/User/)
    rendered.should match(/Email/)
    rendered.should match(/MyText/)
    rendered.should match(/MyText/)
    rendered.should match(/false/)
  end
end
