require 'spec_helper'

describe "user_lexicons/index" do
  before(:each) do
    assign(:user_lexicons, [
      stub_model(UserLexicon,
        :user => "User",
        :email => "Email",
        :issue => "MyText",
        :reply => "MyText",
        :is_active => false
      ),
      stub_model(UserLexicon,
        :user => "User",
        :email => "Email",
        :issue => "MyText",
        :reply => "MyText",
        :is_active => false
      )
    ])
  end

  it "renders a list of user_lexicons" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "User".to_s, :count => 2
    assert_select "tr>td", :text => "Email".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
