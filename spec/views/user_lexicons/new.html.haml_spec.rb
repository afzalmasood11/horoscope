require 'spec_helper'

describe "user_lexicons/new" do
  before(:each) do
    assign(:user_lexicon, stub_model(UserLexicon,
      :user => "MyString",
      :email => "MyString",
      :issue => "MyText",
      :reply => "MyText",
      :is_active => false
    ).as_new_record)
  end

  it "renders new user_lexicon form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", user_lexicons_path, "post" do
      assert_select "input#user_lexicon_user[name=?]", "user_lexicon[user]"
      assert_select "input#user_lexicon_email[name=?]", "user_lexicon[email]"
      assert_select "textarea#user_lexicon_issue[name=?]", "user_lexicon[issue]"
      assert_select "textarea#user_lexicon_reply[name=?]", "user_lexicon[reply]"
      assert_select "input#user_lexicon_is_active[name=?]", "user_lexicon[is_active]"
    end
  end
end
