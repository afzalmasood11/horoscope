require 'spec_helper'

describe "information_pages/edit" do
  before(:each) do
    @information_page = assign(:information_page, stub_model(InformationPage,
      :name => "MyString",
      :title => "MyString",
      :body => "MyText"
    ))
  end

  it "renders the edit information_page form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", information_page_path(@information_page), "post" do
      assert_select "input#information_page_name[name=?]", "information_page[name]"
      assert_select "input#information_page_title[name=?]", "information_page[title]"
      assert_select "textarea#information_page_body[name=?]", "information_page[body]"
    end
  end
end
