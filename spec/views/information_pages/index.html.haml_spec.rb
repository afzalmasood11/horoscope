require 'spec_helper'

describe "information_pages/index" do
  before(:each) do
    assign(:information_pages, [
      stub_model(InformationPage,
        :name => "Name",
        :title => "Title",
        :body => "MyText"
      ),
      stub_model(InformationPage,
        :name => "Name",
        :title => "Title",
        :body => "MyText"
      )
    ])
  end

  it "renders a list of information_pages" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
