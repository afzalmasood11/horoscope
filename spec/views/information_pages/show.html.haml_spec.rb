require 'spec_helper'

describe "information_pages/show" do
  before(:each) do
    @information_page = assign(:information_page, stub_model(InformationPage,
      :name => "Name",
      :title => "Title",
      :body => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(/Title/)
    rendered.should match(/MyText/)
  end
end
