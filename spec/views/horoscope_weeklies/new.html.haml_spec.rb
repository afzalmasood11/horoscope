require 'spec_helper'

describe "horoscope_weeklies/new" do
  before(:each) do
    assign(:horoscope_weekly, stub_model(HoroscopeWeekly,
      :integer => "",
      :published => false
    ).as_new_record)
  end

  it "renders new horoscope_weekly form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", horoscope_weeklies_path, "post" do
      assert_select "input#horoscope_weekly_integer[name=?]", "horoscope_weekly[integer]"
      assert_select "input#horoscope_weekly_published[name=?]", "horoscope_weekly[published]"
    end
  end
end
