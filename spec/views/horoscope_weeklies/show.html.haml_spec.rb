require 'spec_helper'

describe "horoscope_weeklies/show" do
  before(:each) do
    @horoscope_weekly = assign(:horoscope_weekly, stub_model(HoroscopeWeekly,
      :integer => "",
      :published => false
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(/false/)
  end
end
