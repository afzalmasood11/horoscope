require 'spec_helper'

describe "horoscope_weeklies/index" do
  before(:each) do
    assign(:horoscope_weeklies, [
      stub_model(HoroscopeWeekly,
        :integer => "",
        :published => false
      ),
      stub_model(HoroscopeWeekly,
        :integer => "",
        :published => false
      )
    ])
  end

  it "renders a list of horoscope_weeklies" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
