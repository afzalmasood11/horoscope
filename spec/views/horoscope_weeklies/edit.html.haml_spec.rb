require 'spec_helper'

describe "horoscope_weeklies/edit" do
  before(:each) do
    @horoscope_weekly = assign(:horoscope_weekly, stub_model(HoroscopeWeekly,
      :integer => "",
      :published => false
    ))
  end

  it "renders the edit horoscope_weekly form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", horoscope_weekly_path(@horoscope_weekly), "post" do
      assert_select "input#horoscope_weekly_integer[name=?]", "horoscope_weekly[integer]"
      assert_select "input#horoscope_weekly_published[name=?]", "horoscope_weekly[published]"
    end
  end
end
