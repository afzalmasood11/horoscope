require 'spec_helper'

describe "horoscope_monthlies/show" do
  before(:each) do
    @horoscope_monthly = assign(:horoscope_monthly, stub_model(HoroscopeMonthly,
      :zodiac_id => 1,
      :published => false,
      :full_moon => "MyText",
      :quarter_moon_1 => "MyText",
      :new_moon => "MyText",
      :quarter_moon_2 => "MyText",
      :health => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/false/)
    rendered.should match(/MyText/)
    rendered.should match(/MyText/)
    rendered.should match(/MyText/)
    rendered.should match(/MyText/)
    rendered.should match(/MyText/)
  end
end
