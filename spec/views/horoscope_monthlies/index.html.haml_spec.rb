require 'spec_helper'

describe "horoscope_monthlies/index" do
  before(:each) do
    assign(:horoscope_monthlies, [
      stub_model(HoroscopeMonthly,
        :zodiac_id => 1,
        :published => false,
        :full_moon => "MyText",
        :quarter_moon_1 => "MyText",
        :new_moon => "MyText",
        :quarter_moon_2 => "MyText",
        :health => "MyText"
      ),
      stub_model(HoroscopeMonthly,
        :zodiac_id => 1,
        :published => false,
        :full_moon => "MyText",
        :quarter_moon_1 => "MyText",
        :new_moon => "MyText",
        :quarter_moon_2 => "MyText",
        :health => "MyText"
      )
    ])
  end

  it "renders a list of horoscope_monthlies" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
