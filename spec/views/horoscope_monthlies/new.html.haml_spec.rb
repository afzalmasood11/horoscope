require 'spec_helper'

describe "horoscope_monthlies/new" do
  before(:each) do
    assign(:horoscope_monthly, stub_model(HoroscopeMonthly,
      :zodiac_id => 1,
      :published => false,
      :full_moon => "MyText",
      :quarter_moon_1 => "MyText",
      :new_moon => "MyText",
      :quarter_moon_2 => "MyText",
      :health => "MyText"
    ).as_new_record)
  end

  it "renders new horoscope_monthly form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", horoscope_monthlies_path, "post" do
      assert_select "input#horoscope_monthly_zodiac_id[name=?]", "horoscope_monthly[zodiac_id]"
      assert_select "input#horoscope_monthly_published[name=?]", "horoscope_monthly[published]"
      assert_select "textarea#horoscope_monthly_full_moon[name=?]", "horoscope_monthly[full_moon]"
      assert_select "textarea#horoscope_monthly_quarter_moon_1[name=?]", "horoscope_monthly[quarter_moon_1]"
      assert_select "textarea#horoscope_monthly_new_moon[name=?]", "horoscope_monthly[new_moon]"
      assert_select "textarea#horoscope_monthly_quarter_moon_2[name=?]", "horoscope_monthly[quarter_moon_2]"
      assert_select "textarea#horoscope_monthly_health[name=?]", "horoscope_monthly[health]"
    end
  end
end
