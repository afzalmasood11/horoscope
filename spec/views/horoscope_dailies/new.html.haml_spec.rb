require 'spec_helper'

describe "horoscope_dailies/new" do
  before(:each) do
    assign(:horoscope_daily, stub_model(HoroscopeDaily,
      :id => ""
    ).as_new_record)
  end

  it "renders new horoscope_daily form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", horoscope_dailies_path, "post" do
      assert_select "input#horoscope_daily_id[name=?]", "horoscope_daily[id]"
    end
  end
end
