require 'spec_helper'

describe "horoscope_dailies/edit" do
  before(:each) do
    @horoscope_daily = assign(:horoscope_daily, stub_model(HoroscopeDaily,
      :id => ""
    ))
  end

  it "renders the edit horoscope_daily form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", horoscope_daily_path(@horoscope_daily), "post" do
      assert_select "input#horoscope_daily_id[name=?]", "horoscope_daily[id]"
    end
  end
end
