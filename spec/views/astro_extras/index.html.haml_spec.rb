require 'spec_helper'

describe "astro_extras/index" do
  before(:each) do
    assign(:astro_extras, [
      stub_model(AstroExtra,
        :date_from => "Date From",
        :date_to => "Date To",
        :title => "Title",
        :season => "Season",
        :zodiac => "Zodiac",
        :horoscope => "Horoscope",
        :image => "Image",
        :is_active => false
      ),
      stub_model(AstroExtra,
        :date_from => "Date From",
        :date_to => "Date To",
        :title => "Title",
        :season => "Season",
        :zodiac => "Zodiac",
        :horoscope => "Horoscope",
        :image => "Image",
        :is_active => false
      )
    ])
  end

  it "renders a list of astro_extras" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Date From".to_s, :count => 2
    assert_select "tr>td", :text => "Date To".to_s, :count => 2
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "Season".to_s, :count => 2
    assert_select "tr>td", :text => "Zodiac".to_s, :count => 2
    assert_select "tr>td", :text => "Horoscope".to_s, :count => 2
    assert_select "tr>td", :text => "Image".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
