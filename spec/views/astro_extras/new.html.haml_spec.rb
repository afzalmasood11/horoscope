require 'spec_helper'

describe "astro_extras/new" do
  before(:each) do
    assign(:astro_extra, stub_model(AstroExtra,
      :date_from => "MyString",
      :date_to => "MyString",
      :title => "MyString",
      :season => "MyString",
      :zodiac => "MyString",
      :horoscope => "MyString",
      :image => "MyString",
      :is_active => false
    ).as_new_record)
  end

  it "renders new astro_extra form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", astro_extras_path, "post" do
      assert_select "input#astro_extra_date_from[name=?]", "astro_extra[date_from]"
      assert_select "input#astro_extra_date_to[name=?]", "astro_extra[date_to]"
      assert_select "input#astro_extra_title[name=?]", "astro_extra[title]"
      assert_select "input#astro_extra_season[name=?]", "astro_extra[season]"
      assert_select "input#astro_extra_zodiac[name=?]", "astro_extra[zodiac]"
      assert_select "input#astro_extra_horoscope[name=?]", "astro_extra[horoscope]"
      assert_select "input#astro_extra_image[name=?]", "astro_extra[image]"
      assert_select "input#astro_extra_is_active[name=?]", "astro_extra[is_active]"
    end
  end
end
