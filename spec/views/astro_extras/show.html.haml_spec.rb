require 'spec_helper'

describe "astro_extras/show" do
  before(:each) do
    @astro_extra = assign(:astro_extra, stub_model(AstroExtra,
      :date_from => "Date From",
      :date_to => "Date To",
      :title => "Title",
      :season => "Season",
      :zodiac => "Zodiac",
      :horoscope => "Horoscope",
      :image => "Image",
      :is_active => false
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Date From/)
    rendered.should match(/Date To/)
    rendered.should match(/Title/)
    rendered.should match(/Season/)
    rendered.should match(/Zodiac/)
    rendered.should match(/Horoscope/)
    rendered.should match(/Image/)
    rendered.should match(/false/)
  end
end
