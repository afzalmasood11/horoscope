require 'spec_helper'

describe "seasons/edit" do
  before(:each) do
    @season = assign(:season, stub_model(Season,
      :date_from => "MyString",
      :date_to => "MyString"
    ))
  end

  it "renders the edit season form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", season_path(@season), "post" do
      assert_select "input#season_date_from[name=?]", "season[date_from]"
      assert_select "input#season_date_to[name=?]", "season[date_to]"
    end
  end
end
