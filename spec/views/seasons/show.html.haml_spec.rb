require 'spec_helper'

describe "seasons/show" do
  before(:each) do
    @season = assign(:season, stub_model(Season,
      :date_from => "Date From",
      :date_to => "Date To"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Date From/)
    rendered.should match(/Date To/)
  end
end
