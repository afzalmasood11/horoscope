require 'spec_helper'

describe "seasons/index" do
  before(:each) do
    assign(:seasons, [
      stub_model(Season,
        :date_from => "Date From",
        :date_to => "Date To"
      ),
      stub_model(Season,
        :date_from => "Date From",
        :date_to => "Date To"
      )
    ])
  end

  it "renders a list of seasons" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Date From".to_s, :count => 2
    assert_select "tr>td", :text => "Date To".to_s, :count => 2
  end
end
