require 'spec_helper'

describe Api::ValidationsController do

	describe "POST iphone" do
		let(:device){ create(:device) }
		context 'when params are valid' do
			it "should have http status 200 and response body status valid" do
				post :iphone, { device_id: device.device_id, token: device.token, api_key: Conf.get_api_key, receipt_data: get_receipt_data, horoscope_validity: true, method: :collection }
				expect(response.status).to eq(200)
				expect(JSON.parse(response.body)["status"]).to eq('valid')
			end
		end


		context 'when device_id invalid' do
			it "should have status device_id is not valid" do
				post :iphone, { device_id: '', token: device.token, api_key: Conf.get_api_key, receipt_data: get_receipt_data, horoscope_validity: true, method: :collection }
				expect(JSON.parse(response.body)["status"]).to eq('device_id is not valid')
			end
		end

		context 'when all params are not provided' do
			it "should have status invalid parameters" do
				post :iphone
				expect(JSON.parse(response.body)["status"]).to eq('invalid parameters.')
			end
		end
		
		context 'when paramters are invalid' do
			it "should have status invalid parameters" do
				post :iphone, { device_id: device.device_id, token: device.token, api_key: Conf.get_api_key, receipt_data: '090234', horoscope_validity: true, method: :collection }
				expect(JSON.parse(response.body)["status"]).to eq('invalid')
			end
		end

	end

	describe "POST voucher" do
		let(:device){ create(:device) }
		let(:voucher){ create(:voucher, is_active: true) }

		context 'when params are valid' do
      
			it "should return http status 200 and response body status valid" do
				post :voucher, { device_id: device.device_id, token: device.token, api_key: Conf.get_api_key, voucher: voucher.code, method: :collection }
				expect(response.status).to eq(200)
				expect(JSON.parse(response.body)["status"]).to eq('valid')
			end

			it "should create a device voucher" do
				expect do
					post :voucher, { device_id: device.device_id, token: device.token, api_key: Conf.get_api_key, voucher: voucher.code, method: :collection }
				end.to change(DeviceVoucher, :count).by(1)
			end
		end

		context 'when device_id invalid' do
			it "should have status device_id is not valid" do
				post :voucher, { device_id: '', token: device.token, api_key: Conf.get_api_key, voucher: voucher.code, method: :collection }
				expect(JSON.parse(response.body)["status"]).to eq('device_id is not valid')
			end
		end

		context 'when all params are not provided' do
			it "should have status invalid parameters" do
				post :voucher
				expect(JSON.parse(response.body)["status"]).to eq('invalid parameters.')
			end
		end

		context 'when paramters are invalid' do
			it "should have status invalid parameters" do
				post :voucher, { device_id: device.device_id, token: device.token, api_key: Conf.get_api_key, voucher: "3ewe3", method: :collection }
				expect(JSON.parse(response.body)["status"]).to eq('invalid')
			end
		end

	end
	
end
