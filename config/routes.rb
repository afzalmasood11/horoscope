Horoscope::Application.routes.draw do
  
  resources :in_app_purchases

  resources :user_lexicons, except: [:new, :create] do
    collection do
      post :pub_unpub_user_lexicon
    end
  end

  resources :videos do
    collection do
      post :pub_unpub_video
    end
  end

  resources :information_pages  
  match "info/:name" => 'information_page_viewer#show', :as => :view_page
  
  resources :seasons

  resources :astro_extras do
  	collection do
  		post :pub_unpub_astro_extra
  		match :zodiac_names
  	end
  end


  #devise_for :users

  #get "home/index"
  #resources :users

  #devise_for :users, :skip => [:registrations, :sessions]

  #as :user do
  # get "/login" => "devise/sessions#new", :as => :new_user_session
  #post "/login" => "devise/sessions#create", :as => :user_session
  #delete "/logout" => "devise/sessions#destroy", :as => :destroy_user_session
  #end

  #root :to => 'home#index'
  #devise_scope :user do
  #root :to => "devise/sessions#new"
  #end

  namespace :api do
    get "user_lexicon/answers" => "user_lexicons#answers"
    post "user_lexicon/question" => "user_lexicons#create_user_lexicon"
    match "/videos" => "videos#get_videos"
    match "/time" => "server_times#get_server_time"
    match "/astro_extra" => "astro_extras#index"
  	resources :horoscopes
  	match "/token" => "genrate_token#token"
    # resources :contact, :controller => "info_page"
    get "/info" => "info_pages#info"
  	resources :validations do
  		collection do
  			post :iap
  			post :voucher
  		end
  	end
  end

  devise_for :users
  resources :users 
  resources :horoscope_dailies do
  	collection do
  		post :publish_unpub_horoscope
  		match :zodiac_names
  	end
  end
  resources :horoscope_monthlies do
    collection do
  		post :publish_unpub_horoscope
      match :zodiac_names
  	end
  end  
  resources :horoscope_weeklies do
    collection do
  		post :publish_unpub_horoscope      
      match :zodiac_names
  	end
  end
  resources :vouchers do
  	collection do
  		post :pub_unpub_voucher
  	end
  end
  authenticated :user do
  	root :to => 'home#index'
  end
  root :to => "home#index"

  
  
  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
