# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20131004064813) do

  create_table "astro_extras", :force => true do |t|
    t.string   "title"
    t.integer  "zodiac_id"
    t.text     "horoscope"
    t.string   "image"
    t.boolean  "is_active"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "season_id"
  end

  create_table "device_vouchers", :force => true do |t|
    t.string   "device_id"
    t.string   "voucher_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "devices", :force => true do |t|
    t.string   "device_id"
    t.string   "token"
    t.integer  "valid_days",                 :default => 30
    t.boolean  "is_active",                  :default => true
    t.datetime "created_at",                                                    :null => false
    t.datetime "updated_at",                                                    :null => false
    t.datetime "valid_upto",                 :default => '2013-11-08 10:41:23'
    t.date     "horoscope_validity",         :default => '2013-11-08'
    t.date     "rest_of_horoscope_validity", :default => '2013-11-08'
  end

  add_index "devices", ["device_id"], :name => "index_devices_on_device_id", :unique => true

  create_table "devices_iaps", :force => true do |t|
    t.integer  "device_id"
    t.integer  "in_app_purchase_id"
    t.datetime "req_time"
    t.text     "receipt_data"
    t.boolean  "status"
    t.date     "expire_at"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  add_index "devices_iaps", ["device_id"], :name => "index_devices_iaps_on_device_id"
  add_index "devices_iaps", ["in_app_purchase_id"], :name => "index_devices_iaps_on_in_app_purchase_id"

  create_table "horoscope_dailies", :force => true do |t|
    t.integer  "zodiac_id"
    t.date     "date"
    t.boolean  "published",  :default => false
    t.text     "trend"
    t.text     "money"
    t.text     "health"
    t.text     "love"
    t.text     "tip"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
  end

  add_index "horoscope_dailies", ["date"], :name => "index_horoscope_dailies_on_date"

  create_table "horoscope_monthlies", :force => true do |t|
    t.integer  "zodiac_id"
    t.date     "date"
    t.boolean  "published"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "horoscope_monthlies", ["date"], :name => "index_horoscope_monthlies_on_date"

  create_table "horoscope_weeklies", :force => true do |t|
    t.integer  "zodiac_id"
    t.date     "date"
    t.boolean  "published",  :default => false
    t.text     "trends"
    t.text     "dates"
    t.text     "tip"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
  end

  add_index "horoscope_weeklies", ["date"], :name => "index_horoscope_weeklies_on_date_from"

  create_table "in_app_purchases", :force => true do |t|
    t.string   "module_name"
    t.string   "sandbox_id"
    t.string   "production_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "information_pages", :force => true do |t|
    t.string   "name"
    t.string   "title"
    t.text     "body"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.string   "englishname"
    t.text     "url_link"
  end

  create_table "moon_phases", :force => true do |t|
    t.integer  "horoscope_monthly_id"
    t.string   "phase_name"
    t.text     "data"
    t.integer  "priority"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  add_index "moon_phases", ["horoscope_monthly_id"], :name => "index_moon_phases_on_horoscope_monthly_id"

  create_table "roles", :force => true do |t|
    t.string   "name"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "roles", ["name", "resource_type", "resource_id"], :name => "index_roles_on_name_and_resource_type_and_resource_id"
  add_index "roles", ["name"], :name => "index_roles_on_name"

  create_table "seasons", :force => true do |t|
    t.string   "name"
    t.string   "date_from"
    t.string   "date_to"
    t.string   "year"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "seasons", ["name", "year"], :name => "index_seasons_on_name_and_year"

  create_table "user_lexicons", :force => true do |t|
    t.datetime "date"
    t.string   "user"
    t.string   "email"
    t.text     "issue"
    t.text     "reply"
    t.boolean  "is_active"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.string   "username",               :default => "", :null => false
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

  create_table "users_roles", :id => false, :force => true do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "users_roles", ["user_id", "role_id"], :name => "index_users_roles_on_user_id_and_role_id"

  create_table "videos", :force => true do |t|
    t.string   "url"
    t.string   "title"
    t.text     "description"
    t.boolean  "is_active"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "vouchers", :force => true do |t|
    t.string   "code"
    t.integer  "valid_days", :default => 0
    t.boolean  "is_active",  :default => false
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
    t.string   "name"
  end

  create_table "zodiacs", :force => true do |t|
    t.string   "name",       :default => ""
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
    t.string   "zodiac"
  end

end
