class CreateHoroscopeMonthlies < ActiveRecord::Migration
  def change
    create_table :horoscope_monthlies do |t|
      t.integer :zodiac_id
      t.date :date
      t.boolean :published
      t.text :full_moon
      t.text :quarter_moon_1
      t.text :new_moon
      t.text :quarter_moon_2
      t.text :health

      t.timestamps
    end
    add_index :horoscope_monthlies, :date
  end
end
