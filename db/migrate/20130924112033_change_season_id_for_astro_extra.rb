class ChangeSeasonIdForAstroExtra < ActiveRecord::Migration
  
	def up
		ids = []
	 	AstroExtra.all.each do |astro_extra|
	    ids << astro_extra.season_id
	  end

		remove_column :astro_extras, :season_id
		add_column    :astro_extras, :season_id, :integer
    i = 0
    AstroExtra.all.each do |astro_extra|
      astro_extra.update_attribute(:season_id, ids[i].to_i)
      i += 1
    end
  end

  def down
    ids = []
	 	AstroExtra.all.each do |astro_extra|
	    ids << astro_extra.season_id
	  end

		remove_column :astro_extras, :season_id
		add_column    :astro_extras, :season_id, :string
    i = 0
    AstroExtra.all.each do |astro_extra|
      astro_extra.update_attribute(:season_id, ids[i].to_s)
      i += 1
    end
  end


end
