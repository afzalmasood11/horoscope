class ChangeMonthlyHoroscopeTable < ActiveRecord::Migration
	def up
		remove_column :horoscope_monthlies, :full_moon
		remove_column :horoscope_monthlies, :quarter_moon_1
		remove_column :horoscope_monthlies, :new_moon
		remove_column :horoscope_monthlies, :quarter_moon_2
		remove_column :horoscope_monthlies, :health		
	end

	def down
		add_column    :horoscope_monthlies, :full_moon, :text
		add_column    :horoscope_monthlies, :quarter_moon_1, :text
		add_column    :horoscope_monthlies, :new_moon, :text
		add_column    :horoscope_monthlies, :quarter_moon_2, :text
		add_column    :horoscope_monthlies, :health, :text		
	end
end
