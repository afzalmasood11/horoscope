class CreateApiKeys < ActiveRecord::Migration
  def change
    create_table :api_keys do |t|
      t.string :device_id
      t.string :token
      t.integer :valid_days, default: 30
      t.boolean :is_active , default: true

      t.timestamps
    end
    add_index :api_keys, :device_id, unique: true
  end
end
