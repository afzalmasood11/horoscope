class AddValidUptoToApiKeys < ActiveRecord::Migration
  def change
    add_column :api_keys, :valid_upto, :datetime, default: (Time.now + 30.days)
  end
end
