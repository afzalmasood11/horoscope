class ChangeDataTypeOfHoroscopeValidityAndRestOfHoroscopeValidityForDevice < ActiveRecord::Migration
  def up
  	remove_column :devices, :horoscope_validity
  	remove_column :devices, :rest_of_horoscope_validity
		add_column :devices, :horoscope_validity, :date, default: (Date.today + 30.days)
    add_column :devices, :rest_of_horoscope_validity, :date, default: (Date.today + 30.days)
  end

  def down
  	remove_column :devices, :horoscope_validity
  	remove_column :devices, :rest_of_horoscope_validity
  	add_column :devices, :horoscope_validity, :boolean, default: false
  	add_column :devices, :rest_of_horoscope_validity, :boolean, default: false		
  end
end
