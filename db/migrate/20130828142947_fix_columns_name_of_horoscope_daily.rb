class FixColumnsNameOfHoroscopeDaily < ActiveRecord::Migration
  def change
    rename_column :horoscope_dailies, :horoscope_trend, :trend
    rename_column :horoscope_dailies, :horoscope_geld, :money
    rename_column :horoscope_dailies, :horoscope_gesundheit, :health
    rename_column :horoscope_dailies, :horoscope_liebe, :love
    rename_column :horoscope_dailies, :horoscope_tipp, :tip
  end
end
