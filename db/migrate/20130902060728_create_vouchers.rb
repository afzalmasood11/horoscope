class CreateVouchers < ActiveRecord::Migration
  def change
    create_table :vouchers do |t|
      t.string :code
      t.integer :valid_days, default: 0
      t.boolean :is_active, default: false

      t.timestamps
    end
  end
end
