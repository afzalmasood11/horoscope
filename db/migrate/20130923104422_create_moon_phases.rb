class CreateMoonPhases < ActiveRecord::Migration
  def change
    create_table :moon_phases do |t|
      t.references :horoscope_monthly
      t.string :phase_name
      t.text :data
      t.integer :priority

      t.timestamps
    end
    add_index :moon_phases, :horoscope_monthly_id
  end
end
