class CreateDevicesIaps < ActiveRecord::Migration
  def change
    create_table :devices_iaps do |t|
      t.references :device
      t.references :in_app_purchase
      t.datetime :req_time
      t.text :receipt_data
      t.boolean :status
      t.date :expire_at

      t.timestamps
    end
    add_index :devices_iaps, :device_id
    add_index :devices_iaps, :in_app_purchase_id
  end
end
