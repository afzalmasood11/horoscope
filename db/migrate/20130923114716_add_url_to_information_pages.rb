class AddUrlToInformationPages < ActiveRecord::Migration
  def change
    add_column :information_pages, :url_link, :text
  end
end
