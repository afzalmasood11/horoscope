class CreateDeviceVouchers < ActiveRecord::Migration
  def change
    create_table :device_vouchers do |t|
      t.string :device_id
      t.string :voucher_id

      t.timestamps
    end
  end
end
