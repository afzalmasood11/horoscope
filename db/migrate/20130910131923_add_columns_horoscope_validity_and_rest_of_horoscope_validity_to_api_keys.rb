class AddColumnsHoroscopeValidityAndRestOfHoroscopeValidityToApiKeys < ActiveRecord::Migration
  def change
    add_column :api_keys, :horoscope_validity, :boolean
    add_column :api_keys, :rest_of_horoscope_validity, :boolean
  end
end
