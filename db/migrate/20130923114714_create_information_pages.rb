class CreateInformationPages < ActiveRecord::Migration
  def change
    create_table :information_pages do |t|
      t.string :name
      t.string :title
      t.text :body

      t.timestamps
    end
  end
end
