class CreateHoroscopeWeeklies < ActiveRecord::Migration
  def change
    create_table :horoscope_weeklies do |t|
      t.integer :zodiac_id
      t.date :date_from
      t.date :date_to
      t.boolean   :published , :default => FALSE
      t.text :trends
      t.text :dates
      t.text :tip
      t.timestamps
    end
     add_index :horoscope_weeklies, :date_from
     add_index :horoscope_weeklies, :date_to
  end
end
