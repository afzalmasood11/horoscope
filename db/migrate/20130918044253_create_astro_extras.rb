class CreateAstroExtras < ActiveRecord::Migration
  def change
    create_table :astro_extras do |t|
      t.string :title
      t.string :season_id
      t.integer :zodiac_id
      t.string :horoscope
      t.string :image
      t.boolean :is_active

      t.timestamps
    end
  end
end
