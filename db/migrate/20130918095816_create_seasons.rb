class CreateSeasons < ActiveRecord::Migration
  def change
    create_table :seasons do |t|
      t.string :name
      t.string :date_from
      t.string :date_to
      t.string :year
      t.timestamps
    end
    add_index(:seasons, [ :name, :year ])
  end
end
