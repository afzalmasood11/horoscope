class AddEnglishNameToInformationPages < ActiveRecord::Migration
  def change
    add_column :information_pages, :englishname, :string
  end
end
