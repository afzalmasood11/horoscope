class CreateHoroscopeDailies < ActiveRecord::Migration
  def change
    create_table :horoscope_dailies do |t|
      ## Database daily horoscope
      t.integer :zodiac_id    
      t.date :date
      t.boolean   :published , :default => FALSE
      t.text  :horoscope_trend
      t.text :horoscope_geld
      t.text :horoscope_gesundheit
      t.text   :horoscope_liebe
      t.text   :horoscope_tipp
      t.timestamps
    end
    add_index :horoscope_dailies, :date
  end
end
