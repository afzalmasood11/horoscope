class CreateInAppPurchases < ActiveRecord::Migration
  def change
    create_table :in_app_purchases do |t|
      t.string :module_name
      t.string :sandbox_id
      t.string :production_id

      t.timestamps
    end
  end
end
