class CreateUserLexicons < ActiveRecord::Migration
  def change
    create_table :user_lexicons do |t|
      t.datetime :date
      t.string :user
      t.string :email
      t.text :issue
      t.text :reply
      t.boolean :is_active

      t.timestamps
    end
  end
end
