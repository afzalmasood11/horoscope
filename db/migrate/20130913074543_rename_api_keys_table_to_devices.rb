class RenameApiKeysTableToDevices < ActiveRecord::Migration
  def up
    remove_index :api_keys, :device_id
    rename_table :api_keys, :devices
    add_index :devices, :device_id, unique: true
  end

  def down
    remove_index :devices, :device_id
    rename_table :devices, :api_keys
    add_index :api_keys, :device_id, unique: true
  end

end
