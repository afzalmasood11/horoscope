class CreateZodiacs < ActiveRecord::Migration
  def change
    create_table :zodiacs do |t|
      t.string :name, :default => ""

      t.timestamps
    end
  end
end
