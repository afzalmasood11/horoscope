class FixColumnNamesOfWeeklyHoroscope < ActiveRecord::Migration
  def change
    rename_column :horoscope_weeklies, :date_from, :date   
    remove_column :horoscope_weeklies, :date_to
  end
end
