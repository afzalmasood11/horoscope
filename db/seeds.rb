# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Role.create([{name: 'admin'},{name: 'editors'}])
user = User.new(email: 'admin@example.com',  password: 'admin123', password_confirmation: 'admin123', username: 'admin')
user.add_role :admin
user.save(validate: false)
Zodiac.create([{name: 'Widder',zodiac: 'Aries'},{name: 'Stier',zodiac: 'Taurus'},{name: 'Zwillinge',zodiac: 'Twins'},{name: 'Krebs',zodiac: 'Cancer'},{name: 'Löwe',zodiac: 'Lion'},{name: 'Jungfrau',zodiac: 'Virgin'},{name: 'Waage',zodiac: 'Libra'},{name: 'Skorpion',zodiac: 'Scorpio'},{name: 'Schütze',zodiac: 'Sagittarius'},{name: 'Steinbock',zodiac: 'Capricorn'},{name: 'Wassermann',zodiac: 'Aquarius'},{name: 'Fische',zodiac: 'Fish'}])
InformationPage.create([{name: 'mauretania',title: 'Author information',body: '<h1>author information</h1>',englishname: 'author'},{name: 'kontakt',title: 'Kontakt information',body: '<h1>kontakt information</h1>',englishname: 'contact'},{name: 'hilfe',title: 'Help information',body: '<h1>help information</h1>',englishname: 'help'},{name: 'impressum',title: 'Imprint information',body: '<h1>Impressum information</h1>',englishname: 'imprint'}])